package midterm2017;

import java.util.HashMap;
import java.util.Map;

public class question_4c {
		public static void main(String [] args) {
		
			String[] texts = new String[6];
			texts[0] = "Jim Knopf";
			texts[1] = "Avatar";
			texts[2] = "Pocahontas";
			texts[3] = "Avatar";
			texts[4] = "Jim Knopf";
			texts[5] = "Avatar";
			Map<String, Integer> textMap = inventory(texts);
			System.out.println(textMap);

			EBook[] inventory = new EBook[6];
			inventory[0] = new EBook("Jim Knopf", 18, "Children");
			inventory[1] = new EBook("Avatar", 23, "Fantasy");
			inventory[2] = new EBook("Avatar", 23, "Fantasy");
			inventory[3] = new EBook("Pocahontas", 15, "Children");
			inventory[4] = new EBook("Jim Knopf", 23, "Children");
			inventory[5] = new EBook("Avatar", 23, "Fantasy");
			Map<EBook, Integer> booklist = inventory(inventory); 
			System.out.println(booklist);
		}
		
		public static <T> Map<T, Integer> inventory(T[] liste){
			Map<T, Integer> map= new HashMap<T, Integer>();
			for (T x: liste){
				if (map.containsKey(x)){
	 				int counter= map.get(x);
					map.put(x, counter+1);   
				} else {
					map.put(x, 1);
				}
			}
			return map;
		}
	}
