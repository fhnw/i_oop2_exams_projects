package midterm2017;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class LinkedSet<T> implements Iterable<T>, Set<T> {
	private ListElement<T> head = null;
	
	private static class ListElement<T> {
        private T data;
        private ListElement<T> next;
        public ListElement(T data, ListElement<T> next) {
           this.data = data;
           this.next = next;
        }
	}

	/**
	 * Add a new element to the list, enforcing the Set restriction.
	 * Do not add an element if it is already present, or if the new element is "null"
	 * 
	 * @return true, if the element was added.
	 */
	@Override
	public boolean add(T o) {
		// This solution could also use the "contains" method
		// from the Set interface
		boolean found = (o == null); // Do nothing, if o is null
		ListElement<T> cursor = head;
		while (cursor != null & !found) {
			found = cursor.data.equals(o);
			cursor = cursor.next;
		}
		
		if (!found) {
			ListElement<T> newElement = new ListElement<T>(o, head);
			head = newElement;
			return true;
		}
		return false;
	}
	
	/**
	 * Return an element by its position (0 based)
	 * 
	 * @return The element at the given position, or null if no such element exists
	 */
	public T get(int pos) {
		int count = 0;
		T result = null;
		ListElement<T> cursor = head;
		while (cursor != null & count < pos) {
			cursor = cursor.next;
			count++;
		}
		if (cursor != null) result = cursor.data;
		return result;
	}

	@Override
	public boolean remove(Object o) {
		boolean found = false;
		ListElement<T> previous = null;
		ListElement<T> cursor = head;
		while (cursor != null & !found) {
			found = cursor.data.equals(o);
			if (found) {
				if (previous == null) { // head element
					head = cursor.next;
				} else {
					previous.next = cursor.next;
				}
			} else {
				previous = cursor;
				cursor = cursor.next;
			}
		}
		return found;
	}

	@Override
	public int size() {
		int count = 0;
		ListElement<T> cursor = head;
		while (cursor != null) {
			cursor = cursor.next;
			count++;
		}
		return count;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean contains(Object o) {
		boolean found = false;
		ListElement<T> cursor = head;
		while (cursor != null & !found) {
			found = cursor.data.equals(o);
		}
		return found;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean success = true;
		for (Object o : c) {
			success = success & this.remove(o);
		}
		return success;
	}

	@Override
	public void clear() {
		head = null;
	}

	@Override
	public Object[] toArray() {
		return null;
	}

	@Override
	public <TT> TT[] toArray(TT[] a) {
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		return new ListSetIterator<T>();
	}
	
	public class ListSetIterator<U> implements Iterator<U> {
		private ListElement<U> cursor;
		
		public ListSetIterator() {
			cursor = (ListElement<U>) LinkedSet.this.head;
		}

		@Override
		public boolean hasNext() {
			return (cursor != null);
		}

		@Override
		public U next() {
			U data = cursor.data;
			cursor = cursor.next;
			return data;
		}
		
	}
}
