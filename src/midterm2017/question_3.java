package midterm2017;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class question_3 {
	private static ArrayList<EBook> books =
	        new ArrayList<>(Arrays.asList(
	                new EBook("Do you speak Java?", 80, "Development"),
	                new EBook("Business Administration Analysis", 20, "Business"),
	                new EBook("Green Home Building", 152, "Architecture"),
	                new EBook("The Marriage Lie", 52, "Drama")
	        ));

	public static void main(String[] args) {
		q3_1();
		System.out.println();
		System.out.println(q3_2(60));
		System.out.println();
		List<EBook> newList = q3_3();
		newList.forEach(System.out::println);

	}

	// Print all books that are in "Development"
	public static void q3_1() {
		books.stream().filter(b -> b.getCategory().equals("Development")).forEach(System.out::println);
	}
	
	// Count all books with prices less than minPrice
	public static long q3_2(int minPrice) {
		return books.stream().filter(p -> p.getPrice() < minPrice).count();
	}
	
	// Return a new list that contains the books, sorted by their natural order
	public static List<EBook> q3_3() {
		return books.stream().sorted().collect(Collectors.toList());
	}
}
