package midterm2017;

public class EBook implements Comparable<EBook> {
	private String name;
	private int price;
	private String category;
	private boolean isDownloaded;

	public EBook(String name, int price, String category) {
		this.name = name;
		this.price = price;
		this.category = category;
		this.isDownloaded = false;
	}

	/**
	 * The download method downloads and stores a book locally,
	 * and then sets the boolean attribute "isDownloaded".
	 * 
	 * For this exam, we omit the implementation.
	 */
	public void download() {
		// implementation omitted
		isDownloaded = true;
	}

	@Override
	public String toString() {
		return String.format("%s - %s (%d)", name, category, price);
	}

	// compareTo, equals and hashcode use attribute "name"
	@Override
	public int compareTo(EBook p) {
		return this.name.compareTo(p.name);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EBook other = (EBook) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	// Getter methods
	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public String getCategory() {
		return category;
	}

	public boolean isDownloaded() {
		return isDownloaded;
	}
}
