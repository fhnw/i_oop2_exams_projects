package midterm2017;

import java.util.*;

public class question_1 {

	public static void main(String[] args) {
		q1_array(); System.out.println();
		q1_arraylist(); System.out.println();
		q1_treeset(); System.out.println();
		q1_hashmap(); System.out.println();

	}

	public static void q1() {
		q1_array(); System.out.println();
		q1_arraylist(); System.out.println();
		q1_treeset(); System.out.println();
		q1_hashmap(); System.out.println();
	}
	
	public static void q1_array() {
		Integer[] nums = new Integer[6]; // Note the size
		for (int i = 0; i < 5; i++) nums[i] = i % 3;
		
		for (Integer num : nums) System.out.print(num + " ");
	}
	
	public static void q1_arraylist() {
		ArrayList<Integer> nums = new ArrayList<>();
		for (int i = 0; i < 5; i++) nums.add(i % 3);
		
		for (Integer num : nums) System.out.print(num + " ");
	}
	
	public static void q1_treeset() {
		TreeSet<Integer> nums = new TreeSet<>();
		for (int i = 0; i < 5; i++) nums.add(i % 3);
		
		for (Integer num : nums) System.out.print(num + " ");
	}
	
	public static void q1_hashmap() {
		HashMap<Integer, Integer> nums = new HashMap<>();
		for (int i = 0; i < 5; i++) nums.put(i % 3, i);
		
		for (Integer key : nums.keySet()) System.out.print(nums.get(key) + " ");
	}
}
