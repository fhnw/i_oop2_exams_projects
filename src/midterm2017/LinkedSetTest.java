package midterm2017;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class LinkedSetTest {
	LinkedSet<String> names;
	ArrayList<String> names2;

	@Before
	public void setUp() throws Exception {
		names = new LinkedSet<>();
		names.add("Fred");
		names.add("Anna");
		names.add("Sue");
		names.add("Tom");
		names.add("Laura");
		names.add("Steven");
		
		names2 = new ArrayList<>();
		names2.add("Fred");
		names2.add("Anna");
		names2.add("Sue");
		names2.add("Tom");
		names2.add("Laura");
		names2.add("Steven");
	}

	@Test
	public void test1() {
		assert(names.size() == 6);
		names.add("Anna");
		assert(names.size() == 6);	
	}

	@Test
	public void test2() {
		for (String name : names) {
			assertTrue(names2.remove(name));
		}
		assertTrue(names2.isEmpty());
	}

	@Test
	public void test3() {
		for (String name : names2) {
			assertTrue(names.remove(name));
		}
		assertTrue(names.isEmpty());
	}
	
	@Test
	public void test4() {
		assertTrue(names.get(0).equals("Steven"));
		assertTrue(names.get(3).equals("Sue"));
		assertTrue(names.get(7) == null);
	}
}
