package midterm2017;

public class EBookReader<T extends EBook> {
	LinkedSet<T> books = new LinkedSet<>();

	public void add(T book) {
		books.add(book);
	}
	
	/**
	 * Return the book in the collection with the given name.
	 * For this exam, it doesn't matter what happens if no book is found.
	 * 
	 * Use streams and a lambda expression
	 */
	public T getByName(String name) {
		return books.stream().filter(b -> b.getName().equals(name)).findFirst().get();
	}
	
	/**
	 * Download all books (call the "download" method in the EBook class)
	 * 
	 * Use streams and a method reference
	 */
	public void downloadAll() {
		books.stream().forEach(EBook::download);
	}
}
