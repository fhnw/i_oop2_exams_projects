package midterm2017;

public class question_4b {
		// Cannot add items to a wildcard collection
		private static void snippet1() {
//			EBookReader<?> reader = new EBookReader<>();
//			reader.add(new EBook("Jim Knopf", 25, "ChildBook"));
//			reader.downloadAll();
		}

		// ok
		private static void snippet2() {
			EBookReader<PDF> reader = new EBookReader<PDF>();
			reader.add(new PDF("Jim Knopf", 25, "ChildBook"));
			reader.getByName("Jim Knopf");
		}

		// EpubWithDRM is a subclass of Epub
		private static void snippet3() {
//			EBookReader<Epub> reader = new EBookReader<>();
//			reader.add(new Epub("Jim Knopf", 25, "ChildBook"));
//			reader.downloadAll();
//			EpubWithDRM book = reader.getByName("Jim Knopf");
		}

		// EBookReader is restricted to the EBook class hierarchy
		private static void snippet4() {
//			EBookReader<?> reader = new EBookReader<Object>();
//			reader.add(new EBook("Jim Knopf", 25, "ChildBook"));
//			reader.downloadAll();
		}

		// ok
		private static void snippet5() {
			EBookReader<PDF> reader = new EBookReader<>();
			reader.add(new PDF("Jim Knopf", 25, "ChildBook"));
			reader.downloadAll();
			PDF book = reader.getByName("Jim Knopf");
		}
}
