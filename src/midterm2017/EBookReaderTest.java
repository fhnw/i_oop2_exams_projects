package midterm2017;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EBookReaderTest {
	LinkedSet<EBook> books = new LinkedSet<>();

	@Before
	public void setUp() throws Exception {
		books.add(new EBook("Do you speak Java?", 80, "Development"));
		books.add(new EBook("Business Administration Analysis", 20, "Business"));
		books.add(new EBook("Green Home Building", 152, "Architecture"));
		books.add(new EBook("The Marriage Lie", 52, "Drama"));
	}

	@Test
	public void test() {
		EBookReader<EBook> reader = new EBookReader<>();
		for (EBook book : books) reader.add(book);
		
		EBook book = reader.getByName("Green Home Building");
		assertNotNull(book);
		assertEquals(book.getName(), "Green Home Building");
		
		assertFalse(book.isDownloaded());
		reader.downloadAll();
		assertTrue(book.isDownloaded());
	}
}
