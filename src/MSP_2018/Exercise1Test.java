package MSP_2018;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Exercise1Test {
	private List<Tree> trees;
	private Exercise1 e1;
	
	@BeforeEach
	private void createExercise1() {
		System.out.println("e1 created");
		e1 = new Exercise1();
	}
	
	@BeforeEach
	private void createTrees() {
		System.out.println("trees created");
		trees = new ArrayList<>();
		trees.add(new Conifer(0, "Pine", 2014, false));
		trees.add(new Conifer(1, "Fir", 2015, false));
		trees.add(new Conifer(2, "Fir", 2014, false));
		trees.add(new Conifer(3, "Pine", 2015, false));
		trees.add(new Conifer(4, "Pine", 2014, false));
		trees.add(new Conifer(5, "Pine", 2015, false));		
	}
	

	@Test
	void test4a() {
		Map<Integer, List<Tree>> map = e1.statistics(trees);
		assert(map.keySet().size() == 2);
		assert(map.get(2014).size() == 3);
		assert(map.get(2015).size() == 3);
	}

	@Test
	void test4b() {
		Map<Integer, List<Tree>> map = e1.statistics2(trees);
		assert(map.keySet().size() == 2);
		assert(map.get(2014).size() == 3);
		assert(map.get(2015).size() == 3);
	}
}
