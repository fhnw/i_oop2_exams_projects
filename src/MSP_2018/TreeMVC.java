package MSP_2018;

import javafx.application.Application;
import javafx.stage.Stage;

public class TreeMVC extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		TreeModel model = new TreeModel();
		TreeView view = new TreeView(model, stage);
		TreeController controller = new TreeController(model, view);
		stage.show();
	}
}
