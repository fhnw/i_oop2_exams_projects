package MSP_2018;

import java.util.ArrayList;
import java.util.List;

public class Nursery<T extends Tree> {    // 1 Pkt
	private List<T> trees = new ArrayList<>();    // 1 Pkt

    public int size(){                              // 1 Pkt
        return trees.size();
    }

    public boolean inNursery(T tree){                  // 2 Pkt
        return trees.contains(tree);
    }

    public void plant(T tree){                  // 3 Pkt
        if(tree != null && !inNursery(tree)) trees.add(tree);
    }

    public void transplant(T tree, Nursery<T> nursery){  // 3 Pkt
        if(trees.remove(tree))
        	nursery.plant(tree);
    }

    // insgesamt 5 Pkt
    public T sell(int year, String species){                    // 1PT
    	T tree = null;
    	for (T t : trees) {
    		if (t.getPlantingYear() == year && t.getSpecies().equals(species) && !t.isDead() ) {
    			tree = t;   // 2 Pkt für baum finden
    			trees.remove(t);// 1 Pkt
    			break;
    		}
    	}
    	return tree;// 1 Pkt
    }
}
