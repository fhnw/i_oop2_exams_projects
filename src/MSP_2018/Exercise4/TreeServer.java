package MSP_2018.Exercise4;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TreeServer {

	/**
	 * For simplicity, the loop with the ServerSocket is directly in the main
	 * method. The ServerSocket listens on a fixed port 50001.
	 * 
	 * Each connection request from a client is served by a new instance of the
	 * class TreeClient.
	 */
	public static void main(String[] args) {
		ServerSocket listener;
		try {
			listener = new ServerSocket(50001);
			while (true) {
				Socket s = listener.accept();
				TreeClient tc = new TreeClient(s);
				tc.start();
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}
}
