package MSP_2018.Exercise4;

import java.net.Socket;
import java.util.ArrayList;

public class TreeClient extends Thread {
	private Socket socket;
	private final ArrayList<Tree> trees = new ArrayList<>();

	public TreeClient(Socket socket) {
		this.socket = socket;
	}

	/**
	 * Read trees over the network, until the Socket is closed. Trees are stored in
	 * an ArrayList.
	 * 
	 * When the read method of class Tree returns null, we close the Socket. The
	 * method then prints to the console the total number of trees read. The thread
	 * is then finished.
	 */
	@Override
	public void run() {
		// Read trees until we get a null, adding each tree to the list.
		Tree tree = null;
		do {
			tree = Tree.receive(socket);
			if (tree != null) trees.add(tree);
		} while (tree != null);
		System.out.println("Trees received: " + trees.size());
	}

	public ArrayList<Tree> getTrees() {
		return trees;
	}
}
