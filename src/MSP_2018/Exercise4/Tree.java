package MSP_2018.Exercise4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Tree {
	private final int id; // unique, unchangeable ID
	private String species; // e.g. beech, maple, oak
	private int plantingYear; // year in which the tree was planted
	private boolean dead; // 'true' if the tree is dead

	public Tree(int id, String species, int plantingYear, boolean dead) {
		this.id = id;
		this.species = species;
		this.plantingYear = plantingYear;
		this.dead = dead;
	}

	/**
	 * Send a tree as a String; attributes are separated by slashes. For example,
	 * tree number 13 is a pine tree, planted in 2013, and is alive:
	 * 
	 * 13/Pine/2013/false
	 * 
	 * Each tree is sent on a single line; one line is one tree
	 */
	public void send(Socket s) throws IOException {
		String msg = this.id + "/" + this.species + "/" + this.plantingYear + "/" + dead + "\n";
		OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
		out.write(msg);
		out.flush();
	}

	/**
	 * Read a tree from the given socket, using the format described in the "send"
	 * method: Each tree is sent on a single line; one line is one tree
	 * 
	 * In the event of any exception (socket closing, invalid data, etc.), return
	 * null.
	 */
	public static Tree receive(Socket s) {
		Tree tree = null;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String msg = in.readLine();
			if (msg != null) {
				String[] parts = msg.split("/");
				int id = Integer.parseInt(parts[0]);
				String species = parts[1];
				int year = Integer.parseInt(parts[2]);
				boolean dead = Boolean.parseBoolean(parts[3]);
				tree = new Tree(id, species, year, dead);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tree;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Tree)) return false;

		Tree tree = (Tree) o;
		return id == tree.id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	// Getters and Setters, following the usual naming conventions

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public int getPlantingYear() {
		return plantingYear;
	}

	public void setPlantingYear(int plantingYear) {
		this.plantingYear = plantingYear;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public int getId() {
		return id;
	}
}
