package MSP_2018.Exercise4;

import java.net.Socket;

public class Test {
	public static void main(String[] args) {
		Tree t1 = new Tree(13, "Pine", 2011, false);
		Tree t2 = new Tree(17, "Fir", 2012, false);
		Tree t3 = new Tree(23, "Pine", 2013, false);
		Tree t4 = new Tree(42, "Pine", 2014, false);
		Tree t5 = new Tree(51, "Pine", 2015, false);
		Tree t6 = new Tree(12, "Pine", 2016, false);
		
		try {
			Socket s = new Socket("127.0.0.1", 50001);
			t1.send(s);
			Thread.sleep(20);
			t2.send(s);
			Thread.sleep(20);
			t3.send(s);
			Thread.sleep(20);
			t4.send(s);
			Thread.sleep(20);
			t5.send(s);
			Thread.sleep(20);
			t6.send(s);
			s.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
}
