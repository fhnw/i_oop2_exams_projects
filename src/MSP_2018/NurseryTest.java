package MSP_2018;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NurseryTest {

	@Test
	void testInNursery() {
		// given
		Nursery<Conifer> nursery = new Nursery<>();
		Conifer tree1 = new Conifer(0, "Pine", 2017, false);
		Conifer tree2 = new Conifer(1, "Fir", 2016, true);

		// when
		nursery.plant(tree1);

		// then
		assertTrue(nursery.inNursery(tree1));
		assertFalse(nursery.inNursery(tree2));
		assertFalse(nursery.inNursery(null));
	}

	@Test
	void testSell() {
		// given
		Nursery<Conifer> nursery = new Nursery<>();
		Conifer tree1 = new Conifer(0, "Pine", 2017, false);
		Conifer tree2 = new Conifer(1, "Fir", 2016, true);
		Conifer tree3 = new Conifer(2, "Fir", 2016, false);

		nursery.plant(tree1);
		nursery.plant(tree2);
		nursery.plant(tree3);

		// when
		Conifer t = nursery.sell(2016, "Fir");

		// then
		assertEquals(tree3, t); // dead trees cannot be sold
		assertEquals(2, nursery.size());
		assertFalse(nursery.inNursery(t));
	}

}
