package MSP_2018;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class TreeModel {
	    private final SimpleIntegerProperty id             = new SimpleIntegerProperty(42);
	    private final SimpleStringProperty  species        = new SimpleStringProperty("Pine");
	    private final SimpleIntegerProperty plantingYear = new SimpleIntegerProperty(2016);
	    private final SimpleBooleanProperty dead    = new SimpleBooleanProperty(false);

	    public void sell(){
	        // Contents not shown, but does whatever is needed...
	    }

	    // Getter and Setter methods with the usual names
		public SimpleIntegerProperty idProperty() {
			return id;
		}
		
		public int getID() {
			return id.get();
		}

		public SimpleStringProperty speciesProperty() {
			return species;
		}
		
		public String getSpecies() {
			return species.get();
		}
		
		public void setSpecies(String newSpecies) {
			species.set(newSpecies);
		}

		public SimpleIntegerProperty plantingYearProperty() {
			return plantingYear;
		}
		
		public int getPlantingYear() {
			return plantingYear.get();
		}
		
		public void setPlantingYear(int newYear) {
			plantingYear.set(newYear);
		}

		public SimpleBooleanProperty deadProperty() {
			return dead;
		}
	  
		public boolean isDead() {
			return dead.get();
		}
		public void setDead(boolean isDead) {
			dead.set(isDead);
		}
}
