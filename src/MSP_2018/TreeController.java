package MSP_2018;

import javafx.util.converter.NumberStringConverter;

public class TreeController {
	TreeView view;
	TreeModel model;

	public TreeController(TreeModel model, TreeView view) {
		this.view = view;
		this.model = model;

		setupEventHandlers();
		setupValueChangedListeners();
		setupBindings();
	}

	/**
	 * The "sell" button should call the "sell" method in the model
	 */
	private void setupEventHandlers() {
		view.btnSell.setOnAction(e -> model.sell()); // 2pt
	}

	/**
	 * Any time any data is changed, call the method in the view to enable/disable the button
	 */
	private void setupValueChangedListeners() {
		// 3 PT
		model.speciesProperty()
				.addListener((observable, oldValue, newValue) -> view.enableDisableButton());
		model.plantingYearProperty()
				.addListener((observable, oldValue, newValue) -> view.enableDisableButton());
		model.deadProperty()
				.addListener((observable, oldValue, newValue) -> view.enableDisableButton());
	}

	/**
	 * Bind each of the display fields to the appropriate property in the model.
	 * Changes in the model will immediately display. Changes by the user will be
	 * immediately stored.
	 * 
	 * Use uni-directional or bi-directional bindings as appropriate.
	 */
	private void setupBindings() {
		// 2pt (less than DE, because this was not emphasized)
		view.lblIDvalue.textProperty().bind(model.idProperty().asString());
		view.txtSpecies.textProperty().bindBidirectional(model.speciesProperty());
		view.txtPlantingYear.textProperty().bindBidirectional(model.plantingYearProperty(),
				new NumberStringConverter());
		view.chkDead.selectedProperty().bindBidirectional(model.deadProperty());
	}
}
