package MSP_2018;

import static org.junit.jupiter.api.Assumptions.assumingThat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

public class Exercise1 {

	public List<String> allSpecies(List<Tree> trees) {
		return trees.stream()
				.map(t -> t.getSpecies())
				.distinct()
				.sorted(Comparator.naturalOrder())
				.collect(Collectors.toList());
	}
	
	public List<Tree> plantedBefore(List<Tree> trees, int year) {
		return trees.stream()
				.filter(t -> t.getPlantingYear() < year)
				.sorted(Comparator.comparing(Tree::getPlantingYear))
				.collect(Collectors.toList());
	}
	
	public boolean speciesHealthy(List<Tree> trees, String species) {
		return trees.stream()
				.filter(t -> t.getSpecies().equals(species))
				.noneMatch(t -> t.isDead()); // 
	}

	public Map<Integer, List<Tree>> statistics(List<Tree> trees) {
		HashMap<Integer, List<Tree>> map = new HashMap<>();
		for (Tree t : trees) {
			List<Tree> yearList;
			if (!map.containsKey(t.getPlantingYear())) {
				yearList = new ArrayList<>();
				map.put(t.getPlantingYear(), yearList);
			} else {
				yearList = map.get(t.getPlantingYear());
			}
			yearList.add(t);
		}
		return map;
	}
	
	public Map<Integer, List<Tree>> statistics2(List<Tree> trees) {
		return trees.stream().collect(Collectors.groupingBy(Tree::getPlantingYear));
	}
}
