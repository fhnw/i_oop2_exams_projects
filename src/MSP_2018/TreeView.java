package MSP_2018;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TreeView {
	private TreeModel model;

	// Declare all controls here. Hint: declare the controls "package private" to
	// make access from the controller simpler. If you wish, you can initialize the
	// controls here as well.
	Label lblID, lblSpecies, lblPlantingYear, lblDead; // 1pt

	TextField txtSpecies, txtPlantingYear; // 1pt
	Label lblIDvalue; // Could also be a disabled TextField

	CheckBox chkDead; // 1pt
	Button btnSell;

	public TreeView(TreeModel model, Stage stage) {
		this.model = model;

		// Initialize the controls, if not done above
		lblID = new Label("ID"); // 1pt
		lblSpecies = new Label("Species");
		lblPlantingYear = new Label("Planting Year");
		lblDead = new Label("Dead");

		lblIDvalue = new Label(); // 1pt
		txtSpecies = new TextField();
		txtPlantingYear = new TextField();
		chkDead = new CheckBox();
		btnSell = new Button("Sell");

		// Layout controls (3 points)
		Region spacer = new Region();
		VBox root = new VBox(lblID, lblIDvalue, lblSpecies, txtSpecies, lblPlantingYear, txtPlantingYear, lblDead,
				chkDead, spacer, btnSell); // 1pt all added
		root.setSpacing(10); // 0.5pt for spacing between controls
		root.setPadding(new Insets(10)); // 0.5pt for spacing around edge
		VBox.setVgrow(spacer, Priority.ALWAYS); // 1pt for spacer with Vgrow
		btnSell.setMaxWidth(Integer.MAX_VALUE); // 1pt Button always fills width

		enableDisableButton(); // 1pt Initial enable/disable of button

		Scene scene = new Scene(root);
		stage.setScene(scene);
		;
	}

	/**
	 * Check the data stored in the model; enable/disable btnSell as needed
	 */
	void enableDisableButton() {
		// Data must be taken from the model, not the controls (2 points)
		boolean disable = model.getSpecies().isEmpty() || model.isDead() || model.getPlantingYear() > 2018; // Logik: 1
																											// PT
		btnSell.setDisable(disable); // 1pt
	}
}
