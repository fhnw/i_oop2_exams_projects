package MSP_2018;

public class Tree {
	private final int id; // unique, unchangeable ID
	private String species; // e.g. beech, maple, oak
	private int plantingYear; // year in which the tree was planted
	private boolean dead; // 'true' if the tree is dead

	public Tree(int id, String species, int plantingYear, boolean dead) {
		this.id = id;
		this.species = species;
		this.plantingYear = plantingYear;
		this.dead = dead;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Tree)) return false;

		Tree tree = (Tree) o;
		return id == tree.id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	// Getters and Setters, following the usual naming conventions

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public int getPlantingYear() {
		return plantingYear;
	}

	public void setPlantingYear(int plantingYear) {
		this.plantingYear = plantingYear;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public int getId() {
		return id;
	}
}
