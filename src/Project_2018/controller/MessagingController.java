package Project_2018.controller;

import Project_2018.model.Model;
import Project_2018.model.Person;
import Project_2018.view.MessagingPane;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class MessagingController {
	private MessagingPane messagingPane;
	private Model model;
	private SimpleObjectProperty<Person> personProperty;
	private SimpleStringProperty newMessageFrom;

	public MessagingController(Model model, MessagingPane messagingPane) {
		this.model = model;
		this.messagingPane = messagingPane;

		// Selecting a person to communicate with
		personProperty = messagingPane.getPersonProperty();
		personProperty.addListener((val, oldVal, newVal) -> {
			Platform.runLater(() -> {
				messagingPane.getSendButton().setDisable(newVal == null);
				messagingPane.updateConversation();
			});
		});

		// Monitor model for new messages
		newMessageFrom = model.getNewMessageFromProperty();
		newMessageFrom.addListener((val, oldVal, newVal) -> {
			Platform.runLater(() -> {
				// This test should be unnecessary - unknown contacts
				// already caught in the model
				if (newVal != null && newVal.length() > 0) {
					messageReceived(newVal);
					model.clearNewMessageFrom();
				}
			});
		});

		// Sending messages
		messagingPane.getSendButton().setOnAction(e -> doBtnSend(personProperty.get()));
	}

	private void doBtnSend(Person p) {
		model.sendMessage(p, messagingPane.getMessage().getText());
		messagingPane.updateConversation();
	}

	private void messageReceived(String from) {
		// Find person in contacts
		Person p = Person.findByNickname(model.getElements(), from);
		// Should never be null, at this point
		if (p != null) {
			// Select person in table
			messagingPane.selectPerson(p);

			// Display conversation
			messagingPane.updateConversation();
		}
	}
}
