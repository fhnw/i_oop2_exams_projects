package Project_2018.controller;

import Project_2018.model.Model;
import Project_2018.view.View;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import Project_2018.model.Person;

public class Controller {
	public Controller(Model model, View view) {
		// Events for the table
		TableView<Person> tableView = view.getTableView();
		tableView.setOnMouseClicked( e -> {
			MouseButton btn = e.getButton();
			Person p = tableView.getSelectionModel().getSelectedItem();
			if (btn == MouseButton.PRIMARY) {
				view.showMessaging();
				view.setMessaging(p);
			} else if (btn == MouseButton.SECONDARY) {
				view.showEditing();
				if (p != null) { // Normal case
					view.setEditing(p);
				} else { // There are no people at all; must enter new person
					view.getEditingPane().clearAllFields();
					view.getEditingPane().disableAllButNew();
				}
			}
		});
		
		// Create sub-controllers
		new EditingController(model, view.getEditingPane());
		new MessagingController(model, view.getMessagingPane());
		new MenuController(model, view);
	}

}
