package Project_2018.controller;

import Project_2018.model.Model;
import Project_2018.view.Menus;
import Project_2018.view.OptionsDialog;
import Project_2018.view.View;

public class MenuController {
	private Menus menuPane;
	
	public MenuController(Model model, View view) {
		this.menuPane = view.getMenus();
		
		menuPane.getMenuIdentity().setOnAction(event -> {
			OptionsDialog dlg = new OptionsDialog();
			dlg.showAndWait();
			view.updateTitle();
			model.goOnline();
			view.getStatusBar().updateOnlineStatus();
		});
	}	
}
