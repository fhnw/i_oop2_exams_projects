package Project_2018.controller;

import Project_2018.model.Model;
import Project_2018.model.Person;
import Project_2018.view.EditingPane;

public class EditingController {
	private EditingPane editingPane;
	private Model model;
	private boolean addingNewPerson = false;

	public EditingController(Model model, EditingPane editingPane) {
		this.model = model;
		this.editingPane = editingPane;
		
		editingPane.getBtnDelete().setOnAction( e -> onBtnDelete() );
		editingPane.getBtnNew().setOnAction( e -> onBtnNew() );
		editingPane.getBtnCancel().setOnAction( e -> onBtnCancel() );
		editingPane.getBtnSave().setOnAction( e -> onBtnSave() );
	}

	/**
	 * Save the entries into the current person object.
	 * 
	 * TODO: Validation is missing
	 */
	private void onBtnSave() {
		if (addingNewPerson) {
			Person p = new Person(editingPane.getTxtNickname().getText());
			p.setFirstName(editingPane.getTxtFirstName().getText());
			p.setLastName(editingPane.getTxtLastName().getText());
			p.setIpAddress(editingPane.getTxtIpAddress().getText());
			p.setPortNumber(Integer.parseInt(editingPane.getTxtPort().getText()));
			model.addPerson(p);
			editingPane.getTxtID().setText(Integer.toString(p.getID()));
			addingNewPerson = false;
		} else {
			Person p = editingPane.getCurrentPerson();
			p.setNickname(editingPane.getTxtNickname().getText());
			p.setFirstName(editingPane.getTxtFirstName().getText());
			p.setLastName(editingPane.getTxtLastName().getText());
			p.setIpAddress(editingPane.getTxtIpAddress().getText());
			p.setPortNumber(Integer.parseInt(editingPane.getTxtPort().getText()));
		}
		editingPane.enableAll();
	}

	/**
	 * Delete the current person
	 */
	private void onBtnDelete() {
		model.deletePerson(editingPane.getCurrentPerson().getID());
		editingPane.enableAll();
	}

	/**
	 * Cancel any pending changes
	 */
	private void onBtnCancel() {
		addingNewPerson = false;
		editingPane.resetPerson();
		editingPane.enableAll();
	}

	/**
	 * Add a new person. The person will only actually be added to the model when
	 * the "save" button is clicked.
	 */
	private void onBtnNew() {
		editingPane.clearAllFields();
		addingNewPerson = true;
		editingPane.enableAllButNew();
	}
}
