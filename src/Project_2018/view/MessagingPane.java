package Project_2018.view;

import Project_2018.model.Person;
import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class MessagingPane extends VBox {
	private View view;
	private final SimpleObjectProperty<Person> currentPerson = new SimpleObjectProperty<>();

	private Label lblContactAddressLabel = new Label();
	private Label lblMessageLabel = new Label();
	private Label lblContactAddress = new Label();
	private TextArea txtConversation = new TextArea();
	private TextField txtMessage = new TextField();
	private Button btnSend = new Button();

	public MessagingPane(View view) {
		this.view = view;
		this.getStyleClass().add("pane");
		this.getStyleClass().add("vbox");
		
		txtConversation.setEditable(false);
		btnSend.setDisable(true); // Initially, no contact is selected
		
		lblMessageLabel.setMinWidth(Region.USE_PREF_SIZE); // Ensure label displays entire text
		
		HBox topBox = new HBox(lblContactAddressLabel, lblContactAddress);
		topBox.getStyleClass().add("hbox");
		HBox.setHgrow(lblContactAddress,  Priority.ALWAYS);
		
		HBox bottomBox = new HBox(lblMessageLabel, txtMessage, btnSend);
		bottomBox.getStyleClass().add("hbox");
		HBox.setHgrow(txtMessage,  Priority.ALWAYS);

		this.getChildren().addAll(topBox, txtConversation, bottomBox);
		VBox.setVgrow(txtConversation, Priority.ALWAYS);
	}

	protected void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		lblContactAddressLabel.setText(t.getString("messaging.lblContactAddress"));
		lblMessageLabel.setText(t.getString("messaging.lblMessageLabel"));
		btnSend.setText(t.getString("messaging.btnSend"));
	}
	
	public void setPerson(Person p) {
		currentPerson.setValue(p);
		if (p != null) {
			lblContactAddress.setText(p.getNickname() + " @ " + p.getIpAddress() + ":" + p.getPortNumber());
		} else {
			lblContactAddress.setText("---");
		}
	}
	
	public void selectPerson(Person p) {
		view.selectPerson(p);
		setPerson(p);
		updateConversation();
	}
	
	public void updateConversation() {
		txtConversation.setText(currentPerson.get().getConversation());
	}
	
	public Person getPerson() {
		return currentPerson.getValue();
	}
	
	public SimpleObjectProperty<Person> getPersonProperty() {
		return currentPerson;
	}
	
	public TextField getMessage() {
		return txtMessage;
	}
	
	public TextArea getConversation() {
		return txtConversation;
	}
	
	public Button getSendButton() {
		return btnSend;
	}
}
