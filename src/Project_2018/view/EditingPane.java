package Project_2018.view;

import Project_2018.model.Person;
import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class EditingPane extends VBox {
	private Person currentPerson = null;

	private Label lblID = new Label();
	private Label lblNickname = new Label();
	private Label lblFirstName = new Label();
	private Label lblLastName = new Label();
	private Label lblIpAddress = new Label();
	private Label lblPort = new Label();

	private TextField txtID = new TextField();
	private TextField txtNickname = new TextField();
	private TextField txtFirstName = new TextField();
	private TextField txtLastName = new TextField();
	private TextField txtIpAddress = new TextField();
	private TextField txtPort = new TextField();
	
	private Button btnDelete = new Button();
	private Button btnNew = new Button();
	private Button btnCancel = new Button();
	private Button btnSave = new Button();

	public EditingPane(View view) {
		this.getStyleClass().add("pane");
		this.getStyleClass().add("vbox");

		txtID.setEditable(false);

		// Ensure labels display their entire contents
		lblID.setMinWidth(Region.USE_PREF_SIZE);
		lblNickname.setMinWidth(Region.USE_PREF_SIZE);
		lblFirstName.setMinWidth(Region.USE_PREF_SIZE);
		lblLastName.setMinWidth(Region.USE_PREF_SIZE);
		lblIpAddress.setMinWidth(Region.USE_PREF_SIZE);
		lblPort.setMinWidth(Region.USE_PREF_SIZE);

		GridPane grid = new GridPane();
		grid.getStyleClass().add("grid-pane");
		grid.addRow(0, lblID, txtID);
		grid.addRow(1, lblNickname, txtNickname);
		grid.addRow(2, lblFirstName, txtFirstName);
		grid.addRow(3, lblLastName, txtLastName);
		grid.addRow(4, lblIpAddress, txtIpAddress);
		grid.addRow(5, lblPort, txtPort);
		
		HBox buttons = new HBox(btnDelete, btnNew, btnCancel, btnSave);
		buttons.getStyleClass().add("hbox");
		buttons.getStyleClass().add("right-align");
		
		this.getChildren().addAll(grid, buttons);
	}

	protected void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		lblID.setText(t.getString("editing.lblID"));
		lblNickname.setText(t.getString("editing.lblNickname"));
		lblFirstName.setText(t.getString("editing.lblFirstName"));
		lblLastName.setText(t.getString("editing.lblLastName"));
		lblIpAddress.setText(t.getString("editing.lblIpAddress"));
		lblPort.setText(t.getString("editing.lblPort"));
		
		btnDelete.setText(t.getString("editing.btnDelete"));
		btnNew.setText(t.getString("editing.btnNew"));
		btnCancel.setText(t.getString("editing.btnCancel"));
		btnSave.setText(t.getString("editing.btnSave"));
	}
	
	public void setPerson(Person p) {
		this.currentPerson = p;
		resetPerson();
	}
	
	public void resetPerson() {
		txtID.setText(Integer.toString(currentPerson.getID()));
		txtNickname.setText(currentPerson.getNickname());
		txtFirstName.setText(currentPerson.getFirstName());
		txtLastName.setText(currentPerson.getLastName());
		txtIpAddress.setText(currentPerson.getIpAddress());
		txtPort.setText(Integer.toString(currentPerson.getPortNumber()));
	}
	
	public void clearAllFields() {
		txtID.setText("");
		txtNickname.setText("");
		txtFirstName.setText("");
		txtLastName.setText("");
		txtIpAddress.setText("");
		txtPort.setText("");
	}
	
	// --- Enabling/disabling controls ---
	
	public void disableAllButNew() {
		btnCancel.setDisable(true);
		btnSave.setDisable(true);
		btnDelete.setDisable(true);
		btnNew.setDisable(false);
	}

	public void enableAllButNew() {
		btnCancel.setDisable(false);
		btnSave.setDisable(false);
		btnDelete.setDisable(false);	
		btnNew.setDisable(true);	
	}
	
	public void enableAll() {
		btnCancel.setDisable(false);
		btnSave.setDisable(false);
		btnDelete.setDisable(false);	
		btnNew.setDisable(false);	
	}
	
	// --- Getters and Setters ---

	public TextField getTxtID() {
		return txtID;
	}

	public TextField getTxtNickname() {
		return txtNickname;
	}

	public TextField getTxtFirstName() {
		return txtFirstName;
	}

	public TextField getTxtLastName() {
		return txtLastName;
	}

	public TextField getTxtIpAddress() {
		return txtIpAddress;
	}

	public TextField getTxtPort() {
		return txtPort;
	}

	public Button getBtnDelete() {
		return btnDelete;
	}

	public Button getBtnNew() {
		return btnNew;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public Button getBtnSave() {
		return btnSave;
	}
	
	public Person getCurrentPerson() {
		return currentPerson;
	}
}
