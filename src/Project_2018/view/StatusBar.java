package Project_2018.view;

import Project_2018.model.Model;
import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.HBox;

public class StatusBar extends HBox {
	private Model model;
	
	private RadioButton statusOnline = new RadioButton();
	private boolean isOnline;
	
	public StatusBar(Model model) {
		super();
		this.model = model;
		
		this.getChildren().add(statusOnline);
		statusOnline.setDisable(true);
		statusOnline.setId("onlineStatus");
		this.setOffline();

		this.getStyleClass().add("hbox");
		this.getStyleClass().add("right-align");
	}
	
	public void updateOnlineStatus() {
		if (model.getOnlineStatus()) {
			setOnline();
		} else {
			setOffline();
		}
	}
	
	public void setOnline() {
		isOnline = true;
		updateTexts();
	}
	
	public void setOffline() {
		isOnline = false;
		updateTexts();
	}
	
	public void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();
		if (isOnline) {
			statusOnline.setSelected(true);
			statusOnline.setText(t.getString("status.online"));
		} else {
			statusOnline.setSelected(false);
			statusOnline.setText(t.getString("status.offline"));
		}
	}
}
