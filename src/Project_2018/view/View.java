package Project_2018.view;

import Project_2018.model.Model;
import Project_2018.model.Person;
import Project_2018.utility.Configuration;
import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class View {
	Stage primaryStage;
	Model model;
	
	// Panes for display in the GUI
	private Menus menuPane = new Menus(this);
	private StatusBar statusBar;
	private TableView<Person> tableView;
	
	private StackPane region = new StackPane(); // Region to hold one of the swappable panes
	private MessagingPane messagingPane;
	private EditingPane editingPane;

	public View(Model model, Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.model = model;
		
		// Root, menus, status
		BorderPane root = new BorderPane();
		root.setTop(menuPane);
		statusBar = new StatusBar(model);
		root.setBottom(statusBar);
		
		// Center area
		SplitPane centerPane = new SplitPane();
		centerPane.setOrientation(Orientation.HORIZONTAL);
		centerPane.setDividerPositions(0.3f);
		root.setCenter(centerPane);
		
		// Initialize TableView: bind to ObservableList in model
		tableView = createTableView();
		centerPane.getItems().add(tableView);
		centerPane.getItems().add(region);

		// Create the two panes
		messagingPane = new MessagingPane(this);
		editingPane = new EditingPane(this);
		
		// Initially display MessagingPane
		showMessaging();
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("im.css").toExternalForm());
		primaryStage.setScene(scene);
	}

	public void start() {
		statusBar.updateOnlineStatus();
		updateTexts();
		primaryStage.show();
	}
	
	private TableView<Person> createTableView() {
		tableView = new TableView<>();

		// Each column needs a title, and a source of data.
		TableColumn<Person, String> colBinary = new TableColumn<>();
		colBinary.setCellValueFactory(c -> c.getValue().getNicknameProperty());
		tableView.getColumns().add(colBinary);

		TableColumn<Person, Boolean> colHex = new TableColumn<>();
		colHex.setCellValueFactory(p -> p.getValue().getOnlineStatusProperty());
		tableView.getColumns().add(colHex);

		// Finally, attach the tableView to the ObservableList of data
		tableView.setItems(model.getElements());
		
		return tableView;
	}
	
	public void selectPerson(Person p) {
		tableView.getSelectionModel().select(model.getIndexOfPerson(p));
	}

	public void updateTitle() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();
		Configuration c = ServiceLocator.getServiceLocator().getConfiguration();
		String nickname = c.getOption(ServiceLocator.OPTION_NICKNAME);
		String ip = c.getOption(ServiceLocator.OPTION_IP);
		String port = c.getOption(ServiceLocator.OPTION_PORT);
		String identity = nickname + " @ " + ip + ":" + port;
		primaryStage.setTitle(t.getString("program.name") + ": " + identity);		
	}
	
	void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		updateTitle();
		
		// Panes
		menuPane.updateTexts();
		statusBar.updateTexts();
		messagingPane.updateTexts();
		editingPane.updateTexts();
		
		// Table columns
		tableView.getColumns().get(0).setText(t.getString("table.nickname"));
		tableView.getColumns().get(1).setText(t.getString("table.onlineStatus"));
	}
	
	public void showMessaging() {
		ObservableList<Node> kids = region.getChildren();
		if (kids.size() > 0) kids.remove(0);
		kids.add(messagingPane);
	}
	
	public void showEditing() {
		ObservableList<Node> kids = region.getChildren();
		if (kids.size() > 0) kids.remove(0);
		kids.add(editingPane);
	}
	
	public void setMessaging(Person p) {
		messagingPane.setPerson(p);
	}
	
	public void setEditing(Person p) {
		editingPane.setPerson(p);
	}
	
	//--- Getters ---
	
	public TableView<Person> getTableView() {
		return tableView;
	}
	
	public EditingPane getEditingPane() {
		return editingPane;
	}
	
	public MessagingPane getMessagingPane() {
		return messagingPane;
	}
	
	public Menus getMenus() {
		return menuPane;
	}
	
	public StatusBar getStatusBar() {
		return statusBar;
	}
}
