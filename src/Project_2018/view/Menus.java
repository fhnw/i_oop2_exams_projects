package Project_2018.view;

import java.util.Locale;

import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class Menus extends MenuBar {
	private Menu menuOptions;
	private MenuItem menuIdentity;
	private Menu menuOptionsLanguage;

	public Menus(View view) {
		ServiceLocator sl = ServiceLocator.getServiceLocator();

		menuOptions = new Menu();
		menuIdentity = new MenuItem();
		menuOptionsLanguage = new Menu();
		this.getMenus().addAll(menuOptions);
		menuOptions.getItems().add(menuIdentity);
		menuOptions.getItems().add(menuOptionsLanguage);

		// It would be overly complicated to put the language events into the controller,
		// so we just keep them here
		for (Locale locale : sl.getLocales()) {
			MenuItem language = new MenuItem(locale.getLanguage());
			menuOptionsLanguage.getItems().add(language);
			language.setOnAction(event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
				sl.setTranslator(new Translator(locale.getLanguage()));
				view.updateTexts();
			});
		}
	}

	protected void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		menuOptions.setText(t.getString("program.menu.options"));
		menuIdentity.setText(t.getString("program.menu.options.identity"));
		menuOptionsLanguage.setText(t.getString("program.menu.options.language"));
	}
	
	public MenuItem getMenuIdentity() {
		return menuIdentity;
	}
}
