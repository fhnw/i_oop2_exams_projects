package Project_2018.view;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import Project_2018.utility.Configuration;
import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class OptionsDialog extends Dialog<Void> {
	private static Translator t = ServiceLocator.getServiceLocator().getTranslator();
	private static Configuration c = ServiceLocator.getServiceLocator().getConfiguration();

	private Label lblNickname = new Label(t.getString("editing.lblNickname"));
	private Label lblIpAddress = new Label(t.getString("editing.lblIpAddress"));
	private Label lblPortNumber = new Label(t.getString("editing.lblPort"));

	private TextField txtNickname = new TextField(c.getOption(ServiceLocator.OPTION_NICKNAME));
	private TextField txtIpAddress = new TextField(c.getOption(ServiceLocator.OPTION_IP));
	private TextField txtPortNumber = new TextField(c.getOption(ServiceLocator.OPTION_PORT));

	public OptionsDialog() {
		GridPane grid = new GridPane();
		grid.addRow(0, lblNickname, txtNickname);
		grid.addRow(1, lblIpAddress, txtIpAddress);
		grid.addRow(2, lblPortNumber, txtPortNumber);
		getDialogPane().setContent(grid);

		getDialogPane().getButtonTypes().add(ButtonType.OK);
		getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

		// Intercept button click on the OK button, to save options
		Button btnOk = (Button) getDialogPane().lookupButton(ButtonType.OK);
		btnOk.addEventFilter(ActionEvent.ACTION, event -> {
			// TODO validation
			c.setLocalOption(ServiceLocator.OPTION_NICKNAME, txtNickname.getText());
			c.setLocalOption(ServiceLocator.OPTION_IP, txtIpAddress.getText());
			c.setLocalOption(ServiceLocator.OPTION_PORT, txtPortNumber.getText());
		});
	}
}
