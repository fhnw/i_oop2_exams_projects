package Project_2018;

import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import Project_2018.controller.Controller;
import Project_2018.model.Model;
import Project_2018.utility.Configuration;
import Project_2018.utility.ServiceLocator;
import Project_2018.utility.Translator;
import Project_2018.view.View;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class InstantMessenger extends Application {
    private static InstantMessenger mainProgram; // singleton

	private Project_2018.utility.ServiceLocator serviceLocator; // resources, after initialization
	private Model model;
	
	public static void main(String[] args) {
		launch();
	}
	
	/**
	 * Program setup occurs here. No need for a splash screen, because everything
	 * is pretty simple and fast.
	 */
	@Override
	public void init() {
		// Ensure we are a singleton in this JVM
        if (mainProgram == null) {
            mainProgram = this;
        } else {
            Platform.exit();
        }
		
        // Initialize program resources
        serviceLocator = ServiceLocator.getServiceLocator();
        serviceLocator.setLogger(configureLogging());
        serviceLocator.setConfiguration(new Configuration());
        String language = serviceLocator.getConfiguration().getOption("Language");
        serviceLocator.setTranslator(new Translator(language));
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		model = new Model();
		View view = new View(model, primaryStage);
		new Controller(model, view);
		
		model.init(); // Load initial data, go online if possible
		view.start(); // Display GUI
	}
	
	@Override
	public void stop() {
		model.stop();
		serviceLocator.getConfiguration().save();
	}

    /**
     * We create a logger with the name of the application, and attach a file
     * handler to it. All logging should be done using this logger. Messages to
     * this logger will also flow up to the root logger, and from there to the
     * console-handler.
     * 
     * We set the level of the console-handler to "INFO", so that the console
     * only receives the more important messages. The levels of the loggers and
     * the file-handler are set to "FINEST".
     */
    private Logger configureLogging() {
        Logger rootLogger = Logger.getLogger("");
        rootLogger.setLevel(Level.FINEST);

        // By default there is one handler: the console
        Handler[] defaultHandlers = Logger.getLogger("").getHandlers();
        defaultHandlers[0].setLevel(Level.CONFIG);

        // Add our logger
        Logger ourLogger = Logger.getLogger(serviceLocator.getAPP_NAME());
        ourLogger.setLevel(Level.FINEST);
        
        // Add a file handler, putting the rotating files in the tmp directory
        try {
            Handler logHandler = new FileHandler("%t/"
                    + serviceLocator.getAPP_NAME() + "_%u" + "_%g" + ".log",
                    1000000, 9);
            logHandler.setLevel(Level.FINEST);
            ourLogger.addHandler(logHandler);
        } catch (Exception e) { // If we are unable to create log files
            throw new RuntimeException("Unable to initialize log files: "
                    + e.toString());
        }

        return ourLogger;
    }
}
