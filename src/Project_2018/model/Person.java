package Project_2018.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Person implements Serializable {
	private static final long serialVersionUID = 1;

	private static int nextID = 0;

	// Since the properties are displayed in a TableView, they must all be
	// observable properties. Since we must create this in readObject, they
	// cannot be final
	private SimpleIntegerProperty ID = new SimpleIntegerProperty();
	private SimpleStringProperty firstName = new SimpleStringProperty();
	private SimpleStringProperty lastName = new SimpleStringProperty();
	private SimpleStringProperty nickname = new SimpleStringProperty();
	private SimpleStringProperty ipAddress = new SimpleStringProperty();
	private SimpleIntegerProperty portNumber = new SimpleIntegerProperty();
	private SimpleBooleanProperty onlineStatus = new SimpleBooleanProperty();
	private SimpleStringProperty conversation = new SimpleStringProperty("");

	public static Person findByNickname(Collection<Person> people, String nickname) {
		Person result = null;
		for (Iterator<Person> i = people.iterator(); i.hasNext() && result == null; ) {
			Person p = i.next();
			if (p.nickname.get().equals(nickname)) {
				result = p;
			}
		}
		return result;
	}
	
	public Person(String nickname) {
		ID.setValue(nextID++);
		setNickname(nickname);
		setOnlineStatus(false);
	}

	public int getID() {
		return ID.getValue();
	}

	public SimpleIntegerProperty getIDProperty() {
		return ID;
	}

	public String getFirstName() {
		return firstName.getValue();
	}

	public void setFirstName(String s) {
		firstName.setValue(s);
	}

	public SimpleStringProperty getFirstNameProperty() {
		return firstName;
	}

	public String getLastName() {
		return lastName.getValue();
	}

	public void setLastName(String s) {
		lastName.setValue(s);
	}

	public SimpleStringProperty getLastNameProperty() {
		return lastName;
	}

	public String getNickname() {
		return nickname.getValue();
	}

	public void setNickname(String s) {
		nickname.setValue(s);
	}

	public SimpleStringProperty getNicknameProperty() {
		return nickname;
	}

	public String getIpAddress() {
		return ipAddress.getValue();
	}

	public void setIpAddress(String s) {
		ipAddress.setValue(s);
	}

	public SimpleStringProperty getIpAddressProperty() {
		return ipAddress;
	}

	public int getPortNumber() {
		return portNumber.getValue();
	}

	public void setPortNumber(int v) {
		portNumber.setValue(v);
	}

	public SimpleIntegerProperty getPortNumberProperty() {
		return portNumber;
	}

	public boolean getOnlineStatus() {
		return onlineStatus.getValue();
	}

	public void setOnlineStatus(boolean v) {
		onlineStatus.setValue(v);
	}

	public SimpleBooleanProperty getOnlineStatusProperty() {
		return onlineStatus;
	}
	
	public String getConversation() {
		return conversation.get();
	}
	
	public SimpleStringProperty getConversationProperty() {
		return conversation;
	}
	
	public void appendConversation(String msg) {
		conversation.set(conversation.get() + msg + "\n");
	}

	// --- Serializable ---
	// Properties are not serializable; hence, we read and write the actual values
	// that they store. Note that we do not save/restore the conversation with this
	// person

	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeObject(new Integer(ID.getValue()));
		out.writeObject(firstName.getValue());
		out.writeObject(lastName.getValue());
		out.writeObject(nickname.getValue());
		out.writeObject(ipAddress.getValue());
		out.writeObject(new Integer(portNumber.getValue()));
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		ID = new SimpleIntegerProperty();
		firstName = new SimpleStringProperty();
		lastName = new SimpleStringProperty();
		nickname = new SimpleStringProperty();
		ipAddress = new SimpleStringProperty();
		portNumber = new SimpleIntegerProperty();
		onlineStatus = new SimpleBooleanProperty();
		conversation = new SimpleStringProperty();

		ID.setValue((Integer) in.readObject());
		firstName.setValue((String) in.readObject());
		lastName.setValue((String) in.readObject());
		nickname.setValue((String) in.readObject());
		ipAddress.setValue((String) in.readObject());
		portNumber.setValue((Integer) in.readObject());
		onlineStatus.setValue(false);
		conversation.set("");
	}
	
	@Override
	public boolean equals(Object o) {
		boolean result = false;
		if (o instanceof Person) {
			Person p = (Person) o;
			result = (p.getID() == this.getID());
		}
		return result;
	}
}
