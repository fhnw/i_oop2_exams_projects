package Project_2018.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import Project_2018.utility.Configuration;
import Project_2018.utility.ServiceLocator;

public class ServerThread extends Thread {
	ServiceLocator sl = ServiceLocator.getServiceLocator(); 
	Logger logger = sl.getLogger();
	
	private Model model;
	private volatile boolean stop = false;
	private ServerSocket ss;

	public ServerThread(Model model) {
		super("ServerSocket");
		this.model = model;
		
		Configuration c = ServiceLocator.getServiceLocator().getConfiguration();
		
		try {
			int port = Integer.parseInt(c.getOption(ServiceLocator.OPTION_PORT));
			if (port <= 0 || port > 65535) throw new NumberFormatException();
			ss = new ServerSocket(port, 10, null);
			this.start();
		} catch (IOException e) {
			logger.info("Could not open ServerSocket");
		} catch (NumberFormatException e) {
			logger.info("Bad port number");
		}	
	}
	
	@Override
	public void run() {
		logger.info("Server thread starting");
		while (!stop) {
			try (Socket s = ss.accept()) {
				BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
				String msg = in.readLine() + "\n";				
				model.msgReceived(msg);
				ServiceLocator.getServiceLocator().getLogger().info("Message received: " + msg);
			} catch (IOException e) {
				// Nothing to do
			} catch (Exception e) {
				// Nothing to do
			}
		}
		logger.info("Server thread ending");
		try {
			ss.close();
		} catch (IOException e) {
			// Nothing to do
		}
	}

	public void stopServer() {
		this.stop = true;
		try {
			ss.close();
		} catch (IOException e) {
			logger.info("ServerThread:Exception2: " + e.toString());
		}
	}
}
