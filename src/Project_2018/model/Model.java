package Project_2018.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Iterator;
import java.util.logging.Logger;

import Project_2018.utility.Configuration;
import Project_2018.utility.ServiceLocator;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {
	private ServiceLocator sl = ServiceLocator.getServiceLocator(); 
	private Logger logger = sl.getLogger();

	private final ObservableList<Person> elements = FXCollections.observableArrayList();
	private ServerThread serverThread;
	private final SimpleStringProperty newMessageFrom = new SimpleStringProperty();
	
	// getters and setters
	public ObservableList<Person> getElements() {
		return elements;
	}

	public void deletePerson(int id) {
		for (Iterator<Person> i = elements.iterator(); i.hasNext(); ) {
			Person p = i.next();
			if (p.getID() == id) {
				i.remove();
				break;
			}
		}
	}

	public void addPerson(Person p) {
		elements.add(p);
	}
	
	public int getIndexOfPerson(Person p) {
		int result = -1;
		for (int i = 0; result < 0 && i < elements.size(); i++) {
			if (elements.get(i).equals(p)) result = i;
		}
		return result;
	}

	/**
	 * Load data from save-file (if present).
	 */
	public void init() {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(sl.getAPP_NAME() + ".dat"))){
			Integer size = (Integer) in.readObject();
			for (int i = 0; i < size; i++) {
				Object o = in.readObject();
				Person p = (Person) o;
				elements.add(p);
			}
            logger.config("Local data file read");
        } catch (FileNotFoundException e) {
            logger.config("No local data file found");
		} catch (IOException|ClassNotFoundException e1) {
			logger.warning("Error reading data file: " + e1.toString());
		}
		
		// try to go online (if we have a port-number)
		goOnline();
	}

	/**
	 * Save data to file
	 */
	public void stop() {
		ServiceLocator sl = ServiceLocator.getServiceLocator();
		Logger logger = sl.getLogger();
		
		this.goOffline();
		
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(sl.getAPP_NAME() + ".dat"))){
			out.writeObject(new Integer(elements.size()));
			for (Person p : elements) {
				out.writeObject(p);
			}
			out.flush();
            logger.config("Local data file written");
        } catch (FileNotFoundException e) {
            logger.warning("Could not save local data file");
		} catch (IOException e1) {
			logger.warning("Error writing data file");
		}
	}

	public void goOnline() {
		goOffline(); // Kill any existing server thread
		logger.info("Model.online");
		serverThread = new ServerThread(this);
	}
	
	public void goOffline() {
		logger.info("Model.offline");
		if (serverThread != null && serverThread.isAlive()) {
			serverThread.stopServer();
		}
	}

	public boolean getOnlineStatus() {
		return serverThread.isAlive();
	}
	
	public SimpleStringProperty getNewMessageFromProperty() {
		return newMessageFrom;
	}
	
	public synchronized String getNewMessageFrom() {
		return newMessageFrom.get();
	}
	
	private synchronized void setNewMessageFrom(String from) {
		newMessageFrom.set(from);
	}
	
	public synchronized void clearNewMessageFrom() {
		newMessageFrom.set(null);
	}
	
	/**
	 * This method is called when a new message arrives. The message begins with the nickname;
	 * we search out the person in our contacts and add this message to the conversation.
	 * We then set the newMessage property to notify any listeners that a new message has arrived, and from whom.
	 */
	public void msgReceived(String newMessage) {
		String from = newMessage.substring(0, newMessage.indexOf(":"));
		
		Person p = Person.findByNickname(elements, from);
		if (p == null) {
			// TODO: show dialog to user
			logger.warning("Message from unknown person: " + from);
		} else {
			p.appendConversation(newMessage);
			setNewMessageFrom(from);
		}
	}

	/**
	 * This method is called to send a new message to a contact.
	 */
	public void sendMessage(Person p, String text) {
		Configuration c = ServiceLocator.getServiceLocator().getConfiguration();
		String from = c.getOption(ServiceLocator.OPTION_NICKNAME);
		String msg = from + ": " + text + "\n";
		
		try (Socket socket = new Socket()) {
			InetSocketAddress address = new InetSocketAddress(p.getIpAddress(), p.getPortNumber());
			socket.connect(address, 3000);
			OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
			
			out.write(msg);
			out.flush();
			logger.info("Sent message (" + address + "): " + msg);
		} catch (IOException e) {
			ServiceLocator.getServiceLocator().getLogger().warning("Unable to send message: " + e);
			msg = "FAIL - " + msg;
			logger.warning("Failed to send message: " + msg);
		}
		
		p.appendConversation(msg);
	}
}
