package MSP_2019.javaFX;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class View {
	private Stage stage;
	private Model model;
	
	// Declare all controls
	Label lblUserName = new Label("Username");
	Label lblPassword = new Label("Password");
	TextField txtUserName = new TextField();
	PasswordField txtPassword = new PasswordField();
	Label lblAttempts = new Label("Attempts:");
	Label lblNumAttempts = new Label("0"); // This is where the number goes!
	Button btnLogin = new Button("Login");
	
	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;
		
		// Build the GUI here
		GridPane top = new GridPane();
		top.getStyleClass().add("grid-pane");
		top.addRow(0, lblUserName, txtUserName);
		top.addRow(1, lblPassword, txtPassword);
		
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		HBox bottom = new HBox(lblAttempts, lblNumAttempts, spacer, btnLogin);
		bottom.getStyleClass().add("hbox");
		
		VBox root = new VBox(top, bottom);
		root.getStyleClass().add("vbox");
		Scene scene = new Scene(root);
		
		stage.setResizable(false); // Window not resizable
		
		// Boilerplate code
		stage.setTitle("Login Window");
		scene.getStylesheets().add(getClass().getResource("login.css").toExternalForm());
		stage.setScene(scene);
	}

	public void start() {
		stage.show();
	}

	public void stop() {
		stage.hide();
	}
}
