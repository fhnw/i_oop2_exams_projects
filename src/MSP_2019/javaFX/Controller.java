package MSP_2019.javaFX;

import javafx.beans.property.SimpleIntegerProperty;

public class Controller {
	private Model model;
	private View view;

	private final SimpleIntegerProperty failedAttempts = new SimpleIntegerProperty();

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;

		view.btnLogin.disableProperty().bind(
				failedAttempts.greaterThanOrEqualTo(5)
				.or(view.txtUserName.textProperty().isEmpty()
				.or(view.txtPassword.textProperty().isEmpty())));
		
		view.lblNumAttempts.textProperty().bind(failedAttempts.asString());
		
		view.btnLogin.setOnAction( e -> {
			if (model.checkLogin(view.txtUserName.getText(), view.txtPassword.getText()))
				view.stop();
			else
				failedAttempts.setValue(failedAttempts.get() + 1);
		});
	}

}
