package MSP_2019;

import java.util.ArrayList;

public class Exceptions {

	public static void main(String[] args) {
		q1_1();
		q1_2();
		q1_4();
	}

	public static void q1_1() {
		ArrayList<String> names = null;
		try {
			System.out.print("1");
			System.out.println(names.size());
		} catch (Exception e) {
			System.out.print(2);
		} finally {
			System.out.println(3);
		}
	}

	public static void q1_2() {
		int[] nums = { 1, 2, 3 };
		try {
			System.out.print("1");
			System.out.println(nums[4]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.print(2);
		} catch (Exception e) {
			System.out.print(3);
		} finally {
			System.out.println(4);
		}
	}

	public static void q1_3() {
		ArrayList<String> names = null;
		try {
			System.out.print("1");
			System.out.println(names.size());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.print(2);
		} finally {
			System.out.println(3);
		}
	}

	public static void q1_4() {
		int limit = 3;
		try {
			woof(limit);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	public static void woof(int limit) throws Exception {
		for (int i = 1; i < 5; i++) {
			System.out.print(i);
			if (i >= limit) throw new Exception("boom");
		}
	}
}
