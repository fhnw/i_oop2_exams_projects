package MSP_2019;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileQuestion {

	public static List<Integer> countChars(String fileName) {
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {
			return lines.map(s -> s.length())
						.collect(Collectors.toList());
		} catch (IOException e) {
			return null;
		}
	}
	
	public static List<Integer> countChars2(String fileName) {
		File f = new File(fileName);
		try (BufferedReader in = new BufferedReader(new FileReader(f))) {
			List<Integer> nums = new ArrayList<>();
			String s = in.readLine();
			while (s != null) {
				nums.add(s.length());
				s = in.readLine();
			}
			return nums;
		} catch (IOException e) {
			return null;
		}
	}

}
