package MSP_2019;

import java.util.Arrays;
import java.util.List;

public class Director {
	public enum Gender { Male, Female };
	
	private final int ID;
	private String name;
	private Gender gender;
	private List<Company.Country> citizenship;
	
	public Director(int ID) {
		this.ID = ID;
	}
	
	public Director(int ID, String name, Gender gender, Company.Country... citizenship) {
		this.ID = ID;
		this.name = name;
		this.gender = gender;
		this.citizenship = Arrays.asList(citizenship);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public List<Company.Country> getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(List<Company.Country> citizenship) {
		this.citizenship = citizenship;
	}

	public int getID() {
		return ID;
	}
}
