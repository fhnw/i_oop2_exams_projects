package MSP_2019;

import java.util.Arrays;
import java.util.List;

public class Company {
	public enum IndustrySector { Materials, Manufacturing, Service, Knowledge }
	public enum BusinessSize { Small, Medium, Large };
	public enum Country { AT, BE, CH, CN, DE, FR }; // And many more
	
	private final int ID;
	private String name;
	private IndustrySector sector;
	private BusinessSize size;
	private Country headquarters; // Where officially based
	private boolean autonomous; // Not owned by another business
	private List<Director> directors; // Company directors
	
	public Company(int ID) {
		this.ID = ID;
	}
	
	public Company(int ID, String name, IndustrySector sector, BusinessSize size, Country headquarters, boolean autonomous, Director... directors) {
		this.ID = ID;
		this.name = name;
		this.sector = sector;
		this.size = size;
		this.headquarters = headquarters;
		this.autonomous = autonomous;
		this.directors = Arrays.asList(directors);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IndustrySector getSector() {
		return sector;
	}

	public void setSector(IndustrySector sector) {
		this.sector = sector;
	}

	public BusinessSize getSize() {
		return size;
	}

	public void setSize(BusinessSize size) {
		this.size = size;
	}

	public Country getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(Country headquarters) {
		this.headquarters = headquarters;
	}

	public boolean isAutonomous() {
		return autonomous;
	}

	public void setAutonomous(boolean autonomous) {
		this.autonomous = autonomous;
	}

	public List<Director> getDirectors() {
		return directors;
	}

	public void setDirectors(List<Director> directors) {
		this.directors = directors;
	}

	public int getID() {
		return ID;
	}
}
