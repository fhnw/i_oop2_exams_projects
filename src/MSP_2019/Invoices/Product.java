package MSP_2019.Invoices;

import java.math.BigDecimal;

public class Product {
	private final Integer productID;
	private String name;
	private String description;
	private BigDecimal price;
	private float weight;
	
	public Product(Integer productID) {
		this.productID = productID;
	}

	public Product(Integer productID, String name, String description, BigDecimal price) {
		this.productID = productID;
		this.name = name;
		this.price = price;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getProductID() {
		return productID;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
}
