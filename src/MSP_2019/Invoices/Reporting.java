package MSP_2019.Invoices;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reporting {
	
	/** Given a list of invoices, find all hardware and calculate the total
	 * value of all the hardware sold
	 * 
	 * @param invoices - an ArrayList containing invoices with different kinds of products
	 */
	public static BigDecimal totalHardware(ArrayList<Invoice<? extends Product>> invoices) {
		BigDecimal total = BigDecimal.ZERO;
		
		// For all invoices
		for (Invoice<? extends Product> invoice : invoices) {
			
			// For all line items
			for (LineItem<? extends Product> lineItem : invoice.getLineItems()) {
				
				// If this line-item is Hardware or a subclass of Hardware
				if (lineItem.getProduct() instanceof Hardware) {
					BigDecimal lineItemTotal = lineItem.getPrice().multiply(new BigDecimal(lineItem.getQuantity())); 
					total = total.add(lineItemTotal);
				}
			}
		}
		return total;
	}
	
	public static HashMap<Integer, BigDecimal> calculateTurnover(List<Pair<Invoice<? extends Product>, ?>> pairs) {
		HashMap<Integer, BigDecimal> map = new HashMap<>();
		for (Pair<Invoice<? extends Product>, ?> pair : pairs) {
			Integer year = pair.getFirst().getDate().getYear();
			BigDecimal amount = pair.getFirst().getInvoiceTotal();
			
			if (map.containsKey(year)) {
				map.put(year, map.get(year).add(amount));
			} else {
				map.put(year, amount);
			}
		}
		return map;
	}
	
	public static <P extends Product> Map<Integer, Integer> totalSoldArticles(List<Invoice<P>> invoices) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for (Invoice<P> invoice : invoices) {
			for (LineItem<P> lineItem : invoice.getLineItems()) {
				Integer qty = lineItem.getQuantity();
				Integer id = lineItem.getProduct().getProductID();
				if (map.containsKey(id)) {
					map.put(id, map.get(id) + qty);
				} else {
					map.put(id,  qty);
				}
			}
		}
		return map;
	}
}
