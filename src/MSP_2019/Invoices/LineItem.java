package MSP_2019.Invoices;

import java.math.BigDecimal;

/** This class is generic, but can only be used with
 *  classes in the Product class hierarchy */
public class LineItem<P extends Product> {
	private BigDecimal price; // Use BigDecimal to avoid rounding errors
	private Integer quantity;
	
	// Declare the attribute "product"
	private P product;
	
	/** Create a new LineItem with the given data.
	 * Use the product price as the LineItem price */
	public LineItem(P product, Integer quantity) {
		this.product = product;
		this.quantity = quantity;
		this.price = product.getPrice();
	}
	
	//--- Assume all getters and setters are implemented ---
	
	public P getProduct() {
		return product;
	}
	public void setProduct(P product) {
		this.product = product;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}	
}
