package MSP_2019.Invoices;

/**
 * A doubly-linked list that stores products, sorted by price
 */
public class SaleList<E extends Product> {
	private ListElt<E> head = null;
	
	private class ListElt<T> {
		ListElt<T> previous = null;
		ListElt<T> next = null;
		T product;
		
		ListElt(T product) { this.product = product; } 
	}
	
	/**
	 * Count the elements in the list and return this number
	 */
	public int size() {
		int count = 0;
		ListElt<E> cursor = head;
		while (cursor != null) {
			cursor = cursor.next;
			count++;
		}
		return count;
	}

	
	/**
     * Returns the element at the specified position in this list.
     *
     * @param  index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     */
	public E get(int index) {
		if (index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException();
		} else {
			int count = 0;
			ListElt<E> cursor = head;
			while (count < index) {
				cursor = cursor.next;
				count++;
			}
			return cursor.product;
		}
	}
	
	/**
	 * Add a new product at the correct (sorted) location in the list
	 */
	public void add(E product) {
		ListElt<E> newElt = new ListElt<>(product);
		if (head == null || product.getPrice().compareTo(head.product.getPrice()) < 0) {
			newElt.next = head;
			head = newElt;
		} else {
			ListElt<E> cursor = head;
			while (cursor.next != null && cursor.next.product.getPrice().compareTo(product.getPrice()) < 0) {
				cursor = cursor.next;
			}
			// The new element must be inserted *after* the cursor
			newElt.next = cursor.next;
			if (cursor.next != null) cursor.next.previous = newElt;
			newElt.previous = cursor;
			cursor.next = newElt;
		}
	}
}
