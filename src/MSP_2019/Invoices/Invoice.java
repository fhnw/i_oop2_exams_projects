package MSP_2019.Invoices;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/** This class is generic, but can only be used with
 *  classes in the Product class hierarchy */
public class Invoice<P extends Product> {
	private final Integer invoiceID;
	private LocalDateTime date;
	private Integer customerID;

	//--- Monthly sales data
	public static <P extends Product> HashMap<Integer, Integer> totalSoldArticles(List<Invoice<P>> invoices) {
		HashMap<Integer, Integer> totalSales = new HashMap<>();
		for (Invoice<P> invoice : invoices) {
			ArrayList<LineItem<P>> lineItems = invoice.getLineItems();
			for (LineItem<P> li : lineItems) {
				Integer productID = li.getProduct().getProductID();
				if (totalSales.containsKey(productID)) {
					Integer totalQuantity = totalSales.get(productID) + li.getQuantity();
					totalSales.put(productID, totalQuantity);
				} else {
					totalSales.put(productID, li.getQuantity());
				}
			}		
		}
		return totalSales;
	}
	
	// Declare the attribute lineItems as an ArrayList
	private final ArrayList<LineItem<P>> lineItems;
	
	public Invoice(Integer invoiceID) {
		this.invoiceID = invoiceID;
		this.lineItems = new ArrayList<>();
	}
	
	/** Calculate prices using BigDecimal, to avoid rounding errors */
	public BigDecimal getInvoiceTotal() {
		BigDecimal total = BigDecimal.ZERO;
		
		// Loop through all entries in lineItems
		for (LineItem<P> lineItem : lineItems) {
			BigDecimal lineItemTotal = lineItem.getPrice().multiply(new BigDecimal(lineItem.getQuantity())); 
			total = total.add(lineItemTotal);
		}
		return total;
	}
	
	/** Add a new LineItem. Set the lineItem price from the product. */
	public void addLineItem(P product, Integer quantity) {
		LineItem<P> lineItem = new LineItem<>(product, quantity);
		lineItems.add(lineItem);
		
	}
	
	//--- Shipping weight method
	public float shippingWeight() {
		float totalWeight = 0f;
		for (LineItem<P> li : lineItems) {
			totalWeight += li.getProduct().getWeight();
		}
		return totalWeight;
	}
	
	//--- Assume all getters and setters are implemented ---
	
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public Integer getInvoiceID() {
		return invoiceID;
	}

	public ArrayList<LineItem<P>> getLineItems() {
		return lineItems;
	}
}
