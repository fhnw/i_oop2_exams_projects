package MSP_2019.Invoices;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class InvoiceTest {
	private ArrayList<Product> products = new ArrayList<>();
	private Invoice<Hardware> invoice1;
	private Invoice<Software> invoice2;
	private Invoice<Service> invoice3;
	private Invoice<Product> invoice4;
	private ArrayList<Invoice<? extends Product>> invoices = new ArrayList<>();
	
	@BeforeEach
	void createProductsAndInvoices() {
		products.add(new Hardware(0, "Dell Latitude", "", new BigDecimal(1000), 1, "Latitude", "sn1"));
		products.add(new Hardware(1, "Lenovo Thinkpad", "", new BigDecimal(1500), 2, "Thinkpad", "sn2"));
		products.add(new Software(2, "SQL Server Seat", "", new BigDecimal(100), 1, Period.ofYears(1)));
		products.add(new Software(3, "SAP Seat", "", new BigDecimal(1000), 2, Period.ofYears(2)));
		products.add(new Service(4, "Consulting", "", new BigDecimal(100), LocalDate.now(), 13, false));
		products.add(new Service(5, "Installation", "", new BigDecimal(100), LocalDate.now(), 14, true));
		products.add(new Product(6, "Some other product", "", new BigDecimal(333)));
		
		invoice1 = new Invoice<>(1);
		invoice1.addLineItem( (Hardware) products.get(0), 2);
		invoice1.addLineItem( (Hardware) products.get(1), 1);
		invoices.add(invoice1);
		
		invoice2 = new Invoice<>(2);
		invoice2.addLineItem( (Software) products.get(2), 5);
		invoice2.addLineItem( (Software) products.get(3), 3);
		invoices.add(invoice2);
		
		invoice3 = new Invoice<>(3);
		invoice3.addLineItem( (Service) products.get(4), 4);
		invoice3.addLineItem( (Service) products.get(5), 6);
		invoices.add(invoice3);
		
		invoice4 = new Invoice<>(4);
		invoice4.addLineItem( products.get(0), 1);
		invoice4.addLineItem( products.get(2), 1);
		invoice4.addLineItem( products.get(4), 1);
		invoice4.addLineItem( products.get(6), 1);
		invoices.add(invoice4);
	}
	
	@Test
	void testInvoices() {
		assertEquals(invoice1.getInvoiceTotal(), new BigDecimal(3500));
		assertEquals(invoice2.getInvoiceTotal(), new BigDecimal(3500));
		assertEquals(invoice3.getInvoiceTotal(), new BigDecimal(1000));
		assertEquals(invoice4.getInvoiceTotal(), new BigDecimal(1533));
	}

	@Test
	void testReporting() {
		BigDecimal hardwareTotal = Reporting.totalHardware(invoices);
		assertEquals(hardwareTotal, new BigDecimal(4500));
	}
}
