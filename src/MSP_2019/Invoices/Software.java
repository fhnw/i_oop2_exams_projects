package MSP_2019.Invoices;

import java.math.BigDecimal;
import java.time.Period;

public class Software extends Product {
	private Integer  licenseID;
	private Period licensePeriod;

	public Software(Integer ProductID) {
		super(ProductID);
	}
	
	public Software(Integer productID, String name, String description, BigDecimal price, Integer licenseID, Period licensePeriod) {
		super(productID, name, description, price);
		this.licenseID = licenseID;
		this.licensePeriod = licensePeriod;		
	}
	
	public Integer getLicenseID() {
		return licenseID;
	}
	public void setLicenseID(Integer licenseID) {
		this.licenseID = licenseID;
	}
	public Period getLicensePeriod() {
		return licensePeriod;
	}
	public void setLicensePeriod(Period licensePeriod) {
		this.licensePeriod = licensePeriod;
	}
}
