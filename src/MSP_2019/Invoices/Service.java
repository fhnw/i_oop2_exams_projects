package MSP_2019.Invoices;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Service extends Product {
	private LocalDate date;
	private Integer employeeID;
	private Boolean onSite;

	public Service(Integer ProductID) {
		super(ProductID);
	}
	
	public Service(Integer productID, String name, String description, BigDecimal price, LocalDate date, Integer employeeID, boolean onSite) {
		super(productID, name, description, price);
		this.date = date;
		this.employeeID = employeeID;
		this.onSite = onSite;		
	}
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Integer getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}
	public Boolean getOnSite() {
		return onSite;
	}
	public void setOnSite(Boolean onSite) {
		this.onSite = onSite;
	}
}
