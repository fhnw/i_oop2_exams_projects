package MSP_2019.Invoices;

import java.math.BigDecimal;

public class Hardware extends Product {
	private Integer supplierID;
	private String model;
	private String serialNr;

	public Hardware(Integer ProductID) {
		super(ProductID);
	}
	
	public Hardware(Integer productID, String name, String description, BigDecimal price, Integer supplierID, String model, String serialNr) {
		super(productID, name, description, price);
		this.supplierID = supplierID;
		this.model = model;
		this.serialNr = serialNr;
	}
	
	public Integer getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(Integer supplierID) {
		this.supplierID = supplierID;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSerialNr() {
		return serialNr;
	}
	public void setSerialNr(String serialNr) {
		this.serialNr = serialNr;
	}
}
