package MSP_2019.Invoices;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SaleListTest {
	ArrayList<Product> products = new ArrayList<>();
	
	@BeforeEach
	void createProducts() {
		products.add(new Product(1, "a", "b", new BigDecimal(1000)));
		products.add(new Product(2, "c", "d", new BigDecimal(100)));
		products.add(new Product(3, "e", "f", new BigDecimal(10)));
	}

	@Test
	void test() {
		SaleList<Product> list = new SaleList<>();
		assertEquals(list.size(), 0);
		list.add(products.get(0));
		list.add(products.get(2));
		list.add(products.get(1));
		assertEquals(list.size(), 3);
		assertEquals(list.get(0), products.get(2));
		assertEquals(list.get(1), products.get(1));
		assertEquals(list.get(2), products.get(0));
	}

}
