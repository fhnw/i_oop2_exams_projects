package MSP_2019;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import MSP_2019.Company.Country;
import MSP_2019.Company.IndustrySector;
import MSP_2019.Director.Gender;

public class LambdaQuestions {

	// Transformation method
	public String firstHalf(String in) {
		return in.substring(0, in.length()/2);
	}
	
	public void lambda1(List<String> list) {
		// Lambda expression
		list.replaceAll( s -> firstHalf(s)); // in.replaceAll(this::firstHalf);
	}
	
	public void lambda2(List<String> list) {
		// Lambda expression
		list.sort(Comparator.naturalOrder());
	}
	
	public long stream1(List<Company> companies) {
		long count = companies.stream()
				.filter(c -> c.getSector() == IndustrySector.Materials)
				.filter(c -> c.getHeadquarters() == Country.CH)
				.count();
		return count;
	}
	
	public long stream2(List<Company> companies) {
		long count = companies.stream()
				.flatMap(c -> c.getDirectors().stream())
				.distinct()
				.count();
		return count;
	}
	
	// Alternative: how many unique female directors are there in the list??
	public long stream2a(List<Director> directors) {
		long count = directors.stream()
				.distinct()
				.filter(c -> c.getGender().equals(Gender.Female))
				.count();
		return count;
	}
	
	public long stream3(List<Company> companies) {
		long count = companies.stream()
				.map(c -> c.getDirectors().size())
				.filter(s -> s == 3)
				.count();
		return count;
	}
	
	public List<String> stream4(List<Company> companies) {
		return companies.stream()
				.map(c -> c.getName())
				.sorted(Comparator.naturalOrder())
				.collect(Collectors.toList());
	}
	
	public long stream5(List<Company> companies) {
		long count = companies.stream()
				.map(c -> c.getDirectors().stream()
						.anyMatch(d -> d.getCitizenship().contains(Country.CH))
						)
				.filter(b -> b)
				.count();
		return count;
	}
	
	public boolean stream6(List<Company> companies) {
		return companies.stream()
				.flatMap(c -> c.getDirectors().stream())
				.distinct() // optional
				.map(d -> d.getName())
				.anyMatch(n -> n.equals("Tom Hanks"));
	}
	
	// Alternative: does Acme AG have a director "Tom Hanks"
	public boolean stream6a(List<Company> companies) {
		return companies.stream()
				.filter(c -> c.getName().equals("Acme AG"))
				.findFirst()
				.get()
				.getDirectors().stream()
				.anyMatch(d -> d.getName().equals("Tom Hanks"));
	}
	
}
