package MSP_2019;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import MSP_2019.Company.BusinessSize;
import MSP_2019.Company.Country;
import MSP_2019.Company.IndustrySector;
import MSP_2019.Director.Gender;

class LambdaQuestionsTest {
	LambdaQuestions lq = new LambdaQuestions();

	@Test
	void test() {
		List<String> list = Arrays.asList("abc", "defg", "123456");
		lq.lambda1(list);
		assertEquals(list.get(0), "a");
		assertEquals(list.get(1), "de");
		assertEquals(list.get(2), "123");
		
		lq.lambda2(list);
		assertEquals(list.get(0), "123");
		assertEquals(list.get(1), "a");
		assertEquals(list.get(2), "de");
	}

	@Test
	void test2() {
		Director d1 = new Director(1, "Anna Müller", Gender.Female, Company.Country.DE);
		Director d2 = new Director(2, "Thomas Schweizer", Gender.Male, Company.Country.CH);
		Director d3 = new Director(3, "Hannelore Baudin", Gender.Female, Company.Country.FR);
		Director d4 = new Director(4, "Jörg Schmid", Gender.Male, Company.Country.DE);
		Director d5 = new Director(5, "Adrian Richner", Gender.Male, Company.Country.CH);
		
		Company c1 = new Company(1, "Acme AG", IndustrySector.Materials, BusinessSize.Medium, Country.DE, true, d1, d2, d5);
		Company c2 = new Company(2, "Woof GmbH", IndustrySector.Service, BusinessSize.Small, Country.CH, false, d3, d4, d5);
		Company c3 = new Company(3, "HumHum AG", IndustrySector.Manufacturing, BusinessSize.Large, Country.CH, true, d1, d3, d4);
		Company c4 = new Company(4, "YoYo GmbH", IndustrySector.Service, BusinessSize.Medium, Country.CH, true, d2, d4);
		
		List<Company> companies = Arrays.asList(c1, c2, c3, c4);
		List<Director> directors = companies.stream().flatMap(c -> c.getDirectors().stream())
				.collect(Collectors.toList());
		
		assertEquals(lq.stream1(companies), 0);
		assertEquals(lq.stream2(companies), 5);
		assertEquals(lq.stream2a(directors), 2);
		assertEquals(lq.stream3(companies), 3);
		assertEquals(lq.stream4(companies), Arrays.asList("Acme AG", "HumHum AG", "Woof GmbH", "YoYo GmbH"));
		assertEquals(lq.stream5(companies), 3);
		assertEquals(lq.stream6(companies), false);
		assertEquals(lq.stream6a(companies), false);
	}
}
