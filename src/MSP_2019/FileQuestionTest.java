package MSP_2019;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

class FileQuestionTest {
	private final String FILE = "/tmp/test.txt";

	@Test
	void test() {
		List<Integer> list1 = FileQuestion.countChars(FILE);
		List<Integer> list2 = FileQuestion.countChars2(FILE);
		
		assertEquals(list1.size(), list2.size());
		
		Iterator<Integer> i1 = list1.iterator();
		Iterator<Integer> i2 = list2.iterator();
		while (i1.hasNext()) {
			assertEquals(i1.next(), i2.next());
		}
	}

}
