package midterm2019.question2;

import java.util.Collection;
import java.util.Iterator;

public class CircularList<T> implements Collection<T> {
	private ListElt<T> station;
	private int numElts; // How many elements are in the list
	private static int nextID = 0;

	private class ListElt<T> {
		private final int ID;
		private ListElt<T> inFront = null;
		private ListElt<T> inBack = null;
		private T data = null;

		/**
		 * Create a new list element. Note: the parameter is not the ID,
		 * but rather the data stored in the list element
		 */
		private ListElt(T data) {
			this.ID = nextID++;
			this.data = data;
		}
	}
	@Override
	public int size() {
		return numElts;
	}

	@Override
	public boolean isEmpty() {
		return numElts == 0;
	}

	/**
	 * Move all gondolas one position forwards (clockwise in the picture). This
	 * changes which gondolas are at the stations.
	 */
	public void move() {
		station = station.inBack;
	}

	/**
	 * Check through the entire list, to see if the given object is present
	 * in any list element
	 */
	@Override
	public boolean contains(Object o) {
		ListElt<T> cursor = station;
		boolean found = false;
		if (station != null) {
			found = (cursor.data == o);
			while (!found && cursor.inFront != station) {
				cursor = cursor.inFront;
				found = (cursor.data == o);
			}
		}
		return found;
	}

	/**
	 * New data can only be stored at the station. The list element currently at
	 * the station will be _in_front_ of the new element
	 */
	@Override
	public boolean add(T data) {
		ListElt<T> elt = new ListElt<>(data);
		if (station == null) { // This is the only element in the list
			station = elt;
			elt.inBack = elt;
			elt.inFront = elt;
		} else { // There are already elements in the list
			elt.data = data;
			elt.inFront = station;
			elt.inBack = station.inBack;
			elt.inBack.inFront = elt;
			station.inBack = elt;
			station = elt;
		}
		numElts++;
		return true;
	}

	/**
	 * Data can be deleted from anywhere in the list. If the element at the station
	 * is deleted, then the element that was _in_back of the station element will be
	 * at the station 
	 */
	@Override
	public boolean remove(Object o) {
		ListElt<T> cursor = station;
		boolean found = false;
		if (station != null) {
			if (cursor.data == o) { // Delete station element
				// Are there other elements?
				if (cursor.inFront == cursor) { // Nope
					cursor = null;
				} else { // Yes, other elements
					station = cursor.inBack;
					remove(cursor);
				}
				found = true;
			} else { // look through rest of list
				while (!found && cursor.inFront != station) {
					cursor = cursor.inFront;
					found = (cursor.data == o);
					if (found) remove(cursor);
				}
			}
		}
		if (found) numElts--;
		return found;
	}

	private void remove(ListElt<T> cursor) {
		cursor = cursor.inBack;
		cursor.inFront = cursor.inFront.inFront;
		cursor.inFront.inBack = cursor;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	
	
}
