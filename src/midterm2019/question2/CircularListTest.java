package midterm2019.question2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CircularListTest {

	@Test
	void test() {
		CircularList<Integer> nums = new CircularList<>();
		assertTrue(nums.size() == 0);
		assertTrue(nums.add(7));
		assertTrue(nums.size() == 1);
		assertTrue(nums.add(5));
		assertTrue(nums.add(3));
		assertTrue(nums.remove(3)); // Relies on Java caching small Integers!!
		assertFalse(nums.remove(2));
		assertTrue(nums.remove(7));
		assertTrue(nums.remove(5));
		assertTrue(nums.size() == 0);
		assertTrue(nums.add(7));
		assertTrue(nums.add(6));
		assertTrue(nums.add(5));
		assertTrue(nums.add(4));
		assertTrue(nums.add(3));
		assertTrue(nums.add(2));
		assertTrue(nums.size() == 6);
		
	}

}
