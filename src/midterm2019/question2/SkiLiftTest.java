package midterm2019.question2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SkiLiftTest {

	@Test
	void test() {
		SkiLift skilift = new SkiLift(8);
		assertTrue(skilift.isEmpty());
		
		assertTrue(skilift.enterFromRight("sue"));
		skilift.move();
		assertTrue(skilift.enterFromLeft("ann"));
		assertTrue(skilift.enterFromRight("tom"));
		skilift.move();
		assertTrue(skilift.enterFromLeft("sam"));
		skilift.move();
		skilift.move();

		assertFalse(skilift.isEmpty());
		
		assertFalse(skilift.enterFromLeft("anyone"));
		assertEquals(skilift.exitFromLeft(), "sue");
		skilift.move();
		assertFalse(skilift.enterFromRight("anyone"));
		assertEquals(skilift.exitFromRight(), "ann");
		assertEquals(skilift.exitFromLeft(), "tom");
		skilift.move();
		assertEquals(skilift.exitFromRight(), "sam");
		
		assertTrue(skilift.isEmpty());
	}

}
