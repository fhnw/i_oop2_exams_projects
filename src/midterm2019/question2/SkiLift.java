package midterm2019.question2;

public class SkiLift {
	private Gondola atLeftStation;
	private Gondola atRightStation;

	private class Gondola {
		private final int ID;
		private Gondola inFront = null;
		private Gondola inBack = null;
		private String name = null;

		/** Create a new, empty gondola */
		private Gondola(int ID) {
			this.ID = ID;
		}
	}

	/**
	 * Create new gondolas, all linked together. The numbers will be in order, since
	 * this is a new ski lift. Initially, gondola 1 is at the left station, and
	 * gondola number/2+1 is at the right station.
	 */
	public SkiLift(int number) {
		Gondola gBehind = null;
		for (int id = 1; id <= number; id++) {
			// Create new gondola,
			Gondola g = new Gondola(id);

			// Attach to the previously generated gondola, which is behind this one
			g.inBack = gBehind;
			if (gBehind != null) gBehind.inFront = g;

			// Initially gondola 1 is at the left station,
			// and gondola number/2+1 is at the right station
			if (id == 1) atLeftStation = g;
			if (id == number / 2 + 1) atRightStation = g;

			gBehind = g;
		}
		// Hook first and last gondolas together
		atLeftStation.inBack = gBehind;
		gBehind.inFront = atLeftStation;
	}

	/**
	 * The named person gets into the gondola at the left station.
	 * 
	 * @param name  The person getting on
	 * @return true if the person got on; false if the gondola was occupied
	 */
	public boolean enterFromLeft(String name) {
		boolean success = (atLeftStation.name == null);
		if (success) atLeftStation.name = name;
		return success;
	}

	/**
	 * The person in the gondola at the left station gets off; the name in the
	 * gondola is always null after this method is finished
	 * 
	 * @return the name of the person in the gondola; null if no one was there
	 */
	public String exitFromLeft() {
		String name = atLeftStation.name;
		atLeftStation.name = null;
		return name;
	}

	/**
	 * The named person gets into the gondola at the right station.
	 * 
	 * @param name  The person getting on
	 * @return true if the person got on; false if the gondola was occupied
	 */
	public boolean enterFromRight(String name) {
		boolean success = (atRightStation.name == null);
		if (success) atRightStation.name = name;
		return success;
	}

	/**
	 * The person in the gondola at the right station gets off; the name in the
	 * gondola is always null after this method is finished
	 * 
	 * @return the name of the person in the gondola; null if no one was there
	 */
	public String exitFromRight() {
		String name = atRightStation.name;
		atRightStation.name = null;
		return name;
	}
	
	/**
	 * @return true if none of the gondolas are occupied
	 */
	public boolean isEmpty() {
		Gondola g = atLeftStation;
		boolean empty = g.name == null;
		g = g.inFront;
		while (g != atLeftStation) {
			empty &= g.name == null;
			g = g.inFront;
		}
		return empty;
	}

	/**
	 * Move all gondolas one position forwards (clockwise in the picture). This
	 * changes which gondolas are at the stations.
	 */
	public void move() {
		atLeftStation = atLeftStation.inBack;
		atRightStation = atRightStation.inBack;
	}
}
