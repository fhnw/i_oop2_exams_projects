package midterm2019;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class Question1 {
	
	// Our data class is basically a string, but defines Comparable and equals differently
	private static class DataClass implements Comparable<DataClass> {
		private final String data;
		
		public DataClass(String data) { this.data = data; }
		
		@Override
		public boolean equals(Object other) {
			if (other == null || other.getClass() != this.getClass()) return false;
			DataClass dc = (DataClass) other;
			return this.data.length() == dc.data.length();
		}
		
		@Override
		public int hashCode() {
			return 31 * this.data.length();
		}
		
		@Override
		public int compareTo(DataClass dc) {
			return Integer.compare(this.data.length(), dc.data.length());
		}
		
		@Override
		public String toString() {
			return data;
		}
	}

	public static void main(String[] args) {
		DataClass[] data = new DataClass[4];
		data[0] = new DataClass("Anna");
		data[1] = new DataClass("Tom");
		data[2] = new DataClass("Fred");
		data[3] = new DataClass("Sue");

		ArrayList<DataClass> list = new ArrayList<>();
		for (DataClass dc : data) list.add(dc);
		for (DataClass dc : list) System.out.print(dc + " ");
		System.out.println();
		
		TreeSet<DataClass> tree = new TreeSet<>();
		for (DataClass dc : data) tree.add(dc);
		for (DataClass dc : tree) System.out.print(dc + " ");
		System.out.println();
		
		HashSet<DataClass> hset = new HashSet<>();
		for (DataClass dc : data) hset.add(dc);
		for (DataClass dc : hset) System.out.print(dc + " ");
		System.out.println();

		TreeMap<String, DataClass> treeMap = new TreeMap<>();
		for (DataClass dc : data) treeMap.put(dc.data, dc);
		for (String key : treeMap.keySet()) System.out.print(treeMap.get(key) + " ");
		System.out.println();
	}
}
