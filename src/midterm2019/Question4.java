package midterm2019;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import midterm2019.Product.Category;

public class Question4 {
	private static ArrayList<Product> products =
	        new ArrayList<>(Arrays.asList(
	        		new Product("Cheese", 10, Product.Category.Food),
	        		new Product("Milk", 5, Product.Category.Food),
	        		new Product("Shirt", 45, Product.Category.Clothing),
	        		new Product("Apple", 3, Product.Category.Food )
	        ));

	public static void main(String[] args) {
		q3_1();
		System.out.println();
		q3_2();
		System.out.println();
		List<Product> newList = q3_3();
		newList.forEach(System.out::println);
	}

	// Sort the products by name, and print them with  System.out.println
	public static void q3_1() {
		products.stream().sorted(Comparator.comparing(Product::getName)).forEach(System.out::println);
	}
	
	// Count the number of products in the Food category, and return this number
	public static long q3_2() {
		return products.stream().filter(p -> p.getCategory() == Category.Food).count();
	}
	
	// Return a new list containing all products that cost less than 20
	public static List<Product> q3_3() {
		return products.stream().filter(p -> p.getPrice() < 20).collect(Collectors.toList());
	}
}
