package midterm2019;

public class Product implements Comparable<Product> {
	private static int nextID = 0;
	
	public static enum Category { Clothing, Dishes, Food };
	
	private final int ID;
	private String name;
	private int price;
	private Category category;
	private boolean isFragile;

	public Product(String name, int price, Category category) {
		this.ID = nextID++;
		this.name = name;
		this.price = price;
		this.category = category;
		this.isFragile = (category == Category.Dishes);
	}

	@Override
	public String toString() {
		return String.format("%d: %s - %s (%d)", ID, name, category, price);
	}

	// compareTo, equals and hashcode use attribute "ID"
	@Override
	public int compareTo(Product p) {
		return this.ID - p.ID;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean result = false;
		if (o instanceof Product) result = ((Product) o).ID == this.ID;
		return result;
	}

	@Override
	public int hashCode() {
		return this.ID;
	}
	
	// Getter methods
	public int getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public Category getCategory() {
		return category;
	}

	public boolean isFragile() {
		return isFragile;
	}
}
