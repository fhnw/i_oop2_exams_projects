package midterm2018;

public class LineItem<T extends Product> {
	private T product; // Store the product being purchased here
	private int pricePerItem; // Actual price to be paid
	private int quantity; // Quantity of product being purchased
	
	public LineItem(T product, int price, int quantity) {
		this.product = product;
		this.pricePerItem = price;
		this.quantity = quantity;
	}

	public LineItem(T product) {
		this(product, product.getPrice(), 1);
	}

	public T getProduct() {
		return product;
	}

	public int getPricePerItem() {
		return pricePerItem;
	}

	public int getQuantity() {
		return quantity;
	}	
}
