package midterm2018;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ShoppingCart {
	// Declare the ArrayList - careful - this is harder than it looks!
	ArrayList<LineItem<Product>> lineItems = new ArrayList<>();
	
	public void add(LineItem<Product> l) {
		lineItems.add(l);
	}
	
	/**
	 * Use streams to discover if this order contains any fragile products
	 * 
	 * @return true if the order contains a fragile product
	 */
	public boolean isFragile() {
		return lineItems.stream().anyMatch( l -> l.getProduct().isFragile());
//		return lineItems.stream().filter( l -> l.getProduct().isFragile()).count() > 0;
	}
	
	/**
	 * Use streams to discover the total price of the entire order.
	 */
	public int getOrderTotal() {
		return lineItems.stream().mapToInt(l -> l.getQuantity() * l.getPricePerItem()).reduce(0, (a,b) -> a+b);
//		return lineItems.stream().collect(Collectors.summingInt(l -> l.getQuantity() * l.getPricePerItem()));
	}
}
