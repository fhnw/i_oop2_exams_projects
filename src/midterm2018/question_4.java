package midterm2018;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class question_4 {
	private static ArrayList<Product> products =
	        new ArrayList<>(Arrays.asList(
	                new Product("Blue jeans", 50, Product.Category.Clothing),
	                new Product("T-shirt", 30, Product.Category.Clothing),
	                new Product("Carafe", 45, Product.Category.Dishes),
	                new Product("Coffee machine", 250, Product.Category.Appliances)
	        ));

	public static void main(String[] args) {
		System.out.println(q3_1());
		System.out.println();
		q3_2(60);
		System.out.println();
		List<Product> newList = q3_3();
		newList.forEach(System.out::println);
	}

	// Count number of Clothing products
	public static long q3_1() {
		return products.stream().filter(b -> b.getCategory() == Product.Category.Clothing).count();
	}
	
	// Print all products with prices greater than minPrice
	public static void q3_2(int minPrice) {
		products.stream().filter(p -> p.getPrice() > minPrice).forEach(System.out::println);
	}
	
	// Return a new list that contains all products, sorted by alphabetically by name
	public static List<Product> q3_3() {
		return products.stream().sorted((c1, c2) -> c1.getName().compareTo(c2.getName())).collect(Collectors.toList());
		//		return products.stream().sorted(Comparator.comparing(p -> p.getName())).collect(Collectors.toList());
	}
}
