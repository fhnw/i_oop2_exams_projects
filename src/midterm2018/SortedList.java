package midterm2018;

import java.util.AbstractList;

public class SortedList<T extends Comparable<T>> extends AbstractList<T> {
	private ListElement<T> head = null;
	
	private static class ListElement<U> {
        private U data;
        private ListElement<U> next;
        public ListElement(U data, ListElement<U> next) {
           this.data = data;
           this.next = next;
        }
	}

	/**
	 * Add a new element to the list, in the correct (sorted) position. In the case
	 * of duplicates, it does not matter whether the new element is added before or
	 * after the element already present. Return true after adding the element.
	 * 
	 * If the element we are given is null, do nothing and return false.
	 * 
	 * @return true, if the element was added.
	 */
	@Override
	public boolean add(T o) {
		boolean result = false;
		if (o != null) {
			if (head == null || o.compareTo(head.data) < 0) { // Add at very start of list
				ListElement<T> newElt = new ListElement<T>(o, head);
				head = newElt;
			} else { // Search for correct position
				ListElement<T> cursor = head;
				while (cursor.next != null && o.compareTo(cursor.next.data) > 0) {
					cursor = cursor.next;
				}
				// Add after cursor
				ListElement<T> newElt = new ListElement<T>(o, cursor.next);
				cursor.next = newElt;
			}
		}
		return result;
	}
	
	/**
	 * Return an element by its position (0 based)
	 * 
	 * @return The element at the given position, or null if no such element exists
	 */
	public T get(int pos) {
		int count = 0;
		T result = null;
		ListElement<T> cursor = head;
		while (cursor != null & count < pos) {
			cursor = cursor.next;
			count++;
		}
		if (cursor != null) result = cursor.data;
		return result;
	}

	@Override
	public boolean remove(Object o) {
		boolean found = false;
		ListElement<T> previous = null;
		ListElement<T> cursor = head;
		while (cursor != null & !found) {
			found = cursor.data.equals(o);
			if (found) {
				if (previous == null) { // head element
					head = cursor.next;
				} else {
					previous.next = cursor.next;
				}
			} else {
				previous = cursor;
				cursor = cursor.next;
			}
		}
		return found;
	}

	@Override
	public int size() {
		int count = 0;
		ListElement<T> cursor = head;
		while (cursor != null) {
			cursor = cursor.next;
			count++;
		}
		return count;
	}
}
