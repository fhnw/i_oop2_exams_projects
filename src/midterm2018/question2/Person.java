package midterm2018.question2;

import java.util.ArrayList;
import java.util.TreeSet;

public class Person {
	private static int nextID = 0;

	// Attributes about this person
	private final int ID;
	private String name;

	// Declare attribute for father, mother and children.
	// Initialize the ArrayList for the children
	private Person father;
	private Person mother;
	private final ArrayList<Person> children = new ArrayList<>();

	public Person() {
		this.ID = nextID++;
	}

	@Override
	public boolean equals(Object o) {
		boolean result = false;
		if (o instanceof Person) result = ((Person) o).ID == this.ID;
		return result;
	}

	/**
	 * Find and return a list of this person's siblings ("Geschwister"). Two people
	 * are siblings if they have the same mother, the same father, or both.
	 * 
	 * (1) Each person may only appear in the output once.
	 * 
	 * (2) You are not your own sibling; "this" person cannot appear in the output.
	 * 
	 * You may choose the return-type of this method; any data structure in Java
	 * Collections is ok.
	 */
	public TreeSet<Person> getSiblings() {
		TreeSet<Person> siblings = new TreeSet<>();
		if (this.father != null) siblings.addAll(father.children);
		if (this.mother != null) siblings.addAll(mother.children);
		siblings.remove(this);
		return siblings;
	}
}
