package midterm2018;

public class question_5b {

	// Wildcards are ok
	private static void snippet1() {
		Product.Category c = Product.Category.Clothing;
		Product p = new Product("xyz", 123, c);
		LineItem<?> lineItem = new LineItem<>(p, 123, 1);
		Stuff s = new Stuff("abc", 234, c);
		LineItem<?> lineItem2 = new LineItem<>(s, 234, 2);
	}

	// Cannot put superclass into subclass
	private static void snippet2() {
		Product.Category c = Product.Category.Clothing;
		Product p = new Product("xyz", 123, c);
//		LineItem<Stuff> lineItem = new LineItem<>(p, 123, 1);
	}

	// Subclass ok
	private static void snippet3() {
		Product.Category c = Product.Category.Clothing;
		Stuff s = new Stuff("xyz", 123, c);
		LineItem<Product> lineItem = new LineItem<>(s, 123, 1);
	}

	// No classes outside hierarchy
	private static void snippet4() {
		Product.Category c = Product.Category.Clothing;
		Integer i = 13;
//		LineItem<?> lineItem = new LineItem<>(i, 123, 1);
	}

	// No classes outside hierarchy
	private static void snippet5() {
		Product.Category c = Product.Category.Clothing;
		Object o = new Product("xyz", 123, c);
//		LineItem<?> lineItem = new LineItem<>(o, 123, 1);
	}
}
