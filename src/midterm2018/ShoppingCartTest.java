package midterm2018;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class ShoppingCartTest {
	private static ArrayList<Product> products;

	@Test
	public void test1() throws Exception {
		products = new ArrayList<>();
		products.add(new Product("Blue jeans", 50, Product.Category.Clothing));
		products.add(new Product("T-shirt", 30, Product.Category.Clothing));
		products.add(new Product("Carafe", 45, Product.Category.Dishes));
		products.add(new Product("Coffee machine", 250, Product.Category.Appliances));
		
		ShoppingCart sc = new ShoppingCart();
		for (int i = 0; i < products.size(); i++) {
			Product p = products.get(i);
			sc.add(new LineItem<Product>(p, p.getPrice() / (i+1), i+1));
		}
		
		assertTrue(sc.isFragile());
	}
	
	
	@Test
	public void test2() throws Exception {
		products = new ArrayList<>();
		products.add(new Product("Blue jeans", 50, Product.Category.Clothing));
		products.add(new Product("T-shirt", 30, Product.Category.Clothing));
		products.add(new Product("Carafe", 45, Product.Category.Dishes));
		products.add(new Product("Coffee machine", 250, Product.Category.Appliances));
		
		ShoppingCart sc = new ShoppingCart();
		for (int i = 0; i < products.size(); i++) {
			Product p = products.get(i);
			sc.add(new LineItem<Product>(p, p.getPrice() / (i+1), i+1));
		}

		assertEquals(sc.getOrderTotal(), 373);
	}
}
