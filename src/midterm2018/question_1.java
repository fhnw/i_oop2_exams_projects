package midterm2018;

import java.util.*;

public class question_1 {
	private final static String[] data = { "Fred", "Sue", "Chris" };

	public static void main(String[] args) {
		q1_array(); System.out.println();
		q1_arraylist(); System.out.println();
		q1_treeset(); System.out.println();
		q1_hashset(); System.out.println();

	}

	public static void q1() {
		q1_array(); System.out.println();
		q1_arraylist(); System.out.println();
		q1_treeset(); System.out.println();
		q1_hashset(); System.out.println();
	}
	
	public static void q1_array() {
		String[] names = new String[5]; // Note the size!!
		for (int i = 0; i < data.length; i++) names[i] = data[i];
		
		for (String name : names) System.out.print(name + " ");
	}
	
	public static void q1_arraylist() {
		ArrayList<String> names = new ArrayList<>();
		for (int i = 0; i < data.length; i++) names.add(data[i]);
		
		for (String name : names) System.out.print(name + " ");
	}
	
	public static void q1_treeset() {
		TreeSet<String> names = new TreeSet<>();
		for (int i = 0; i < data.length; i++) names.add(data[i]);
		
		for (String name : names) System.out.print(name + " ");
	}
	
	public static void q1_hashset() {
		HashSet<String> names = new HashSet<>();
		for (int i = 0; i < data.length; i++) names.add(data[i]);
		
		for (String name : names) System.out.print(name + " ");
	}
}
